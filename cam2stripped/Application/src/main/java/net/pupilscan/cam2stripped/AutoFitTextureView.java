//  AutoFitTextureView.java

//  TextureView with custom aspect ratio

package net.pupilscan.cam2stripped;

import android.content.Context;
import android.util.AttributeSet;
import android.view.TextureView;

public class AutoFitTextureView extends TextureView {

    //  instance variables

    private int mRatioWidth = 0;
    private int mRatioHeight = 0;

    //  constructors

    public AutoFitTextureView(Context context, AttributeSet attrs, int defStyle) {
        //  call superclass method
        super(context, attrs, defStyle);
    }

    public AutoFitTextureView(Context context, AttributeSet attrs) {
        //  call constructor with default values
        this(context, attrs, 0);
    }

    public AutoFitTextureView(Context context) {
        //  call constructor with default values
        this(context, null);
    }

    //  methods

    public void setAspectRatio(int width, int height) throws IllegalArgumentException {
        //  check argument values
        if ((width >= 0) && (height >= 0)) {
            //  remember aspect values
            mRatioWidth = width;
            mRatioHeight = height;
            //  update layout
            this.requestLayout();
        } else
            throw new IllegalArgumentException("Size cannot be negative.");
    }

    //  overrides

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //  call superclass method
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //  get current view size
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        //  check arguments
        if ((mRatioWidth == 0) || (mRatioHeight == 0))
            //  just use default dimensions
            this.setMeasuredDimension(width, height);
        else if (width * mRatioHeight < height * mRatioWidth)
            //  width is proportionally less than height so crop top/bottom
            this.setMeasuredDimension(width, mRatioHeight * width / mRatioWidth);
        else
            //  width is proportionally more than height so crop left/right
            this.setMeasuredDimension(mRatioWidth * height / mRatioHeight, height);
    }
}
