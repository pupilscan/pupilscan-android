//  ConfirmationDialog.java

//  creates and presents confirmation dialog with OK and Cancel buttons

package net.pupilscan.cam2stripped;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;

public class ConfirmationDialog extends DialogFragment {

    //  constants

    private static final String BUNDLE_MESSAGE = "confirmation_dialog_message";
    private static final String CONFIRMATION_DIALOG_TAG = "confirmation_dialog";
    private static final String TAG = "ConfirmationDialog";

    //  listener interface

    public interface OnConfirmationListener {
        public void ok();
        public void cancel();
    }

    //  instance variables

    private OnConfirmationListener mOnConfirmationListener = null;

    //  static methods

    public static void show(String message, FragmentManager fragmentManager, OnConfirmationListener onConfirmationListener) {
        //  create ErrorDialog instance
        ConfirmationDialog confirmationDialog = new ConfirmationDialog();
        //  attach arguments to dialog
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_MESSAGE, message);
        confirmationDialog.setArguments(bundle);
        //  attach listener
        if (onConfirmationListener != null)
            confirmationDialog.mOnConfirmationListener = onConfirmationListener;
        //  display dialog
        confirmationDialog.show(fragmentManager, CONFIRMATION_DIALOG_TAG);
    }

    public static void show(String message, FragmentManager fragmentManager) {
        //  show ErrorDialog
        show(message, fragmentManager, null);
    }

    //  overrides

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //  remember parent fragment
        final Fragment parent = this.getParentFragment();
        //  create confirmation dialog with message and OK+Cancel buttons
        return new AlertDialog.Builder(getActivity())
                .setMessage(getArguments().getString(BUNDLE_MESSAGE))
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            //  overrides
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //  ok callback
                                if (mOnConfirmationListener != null)
                                    mOnConfirmationListener.ok();
                            }
                        })
                .setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            //  overrides
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //  dismiss this dialog
                                parent.getActivity().finish();
                                //  cancel callback
                                if (mOnConfirmationListener != null)
                                    mOnConfirmationListener.cancel();
                            }
                        })
                .create();
    }
}