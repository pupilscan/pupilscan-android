//  CameraActivity.java

//  activity that references the container layout

package net.pupilscan.cam2stripped;

import android.app.Activity;
import android.os.Bundle;

public class CameraActivity extends Activity {

    //  overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  call superclass method
        super.onCreate(savedInstanceState);
        //  set content view using the container layout
        this.setContentView(R.layout.layout_container);
        //  check savedInstanceState
        if (savedInstanceState == null)
            //  load contained fragments
            this.getFragmentManager().beginTransaction()
                    .replace(R.id.container, Camera2VideoFragment.newInstance())
                    .commit();
    }
}
