//  Settings.java

//  persistent storage for application settings

package net.pupilscan.cam2stripped;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Settings {

    /*
        from PupilScan iOS app:
        static let authorizedKey = "authorized"
        static let badgeKey = "badge"
        static let databaseKey = "database"
        static let locationKey = "location"
        static let overlayKey = "overlay"
        static let patientKey = "patient"
        static let pilotKey = "pilot"
        static let speechKey = "speech"
    */

    //  constants

    private static final String PREFERENCES_NAME = "PupilScanPreferences";
    private static final String KEY_ATTEST = "Development";
    private static final String KEY_AUDIO= "Audio";
    private static final String KEY_DEVELOPMENT = "Development";
    private static final String KEY_FRONT_CAMERA = "FrontCamera";

    //  singleton

    public static Settings shared = new Settings();

    //  instance variables

    //private Boolean mDevelopment = false;

    //  constructor

    private Settings() {
        //  empty for now
    }

    //  public getters and setters

    public Integer getAttest() {
        return this.getPref().getInt(KEY_ATTEST, 0);
    }
    public void setAttest(Integer attest) {
        this.getEditor().putInt(KEY_ATTEST, attest);
    }
    public Boolean getAudio() {
        return this.getPref().getBoolean(KEY_AUDIO, false);
    }
    public void setAudio(Boolean audio) {
        this.getEditor().putBoolean(KEY_AUDIO, audio);
    }
    public Boolean getDevelopment() {
        return this.getPref().getBoolean(KEY_DEVELOPMENT, false);
    }
    public void setDevelopment(Boolean development) {
        this.getEditor().putBoolean(KEY_DEVELOPMENT, development);
    }
    public Boolean getFrontCamera() {
        return this.getPref().getBoolean(KEY_FRONT_CAMERA, false);
    }
    public void setFrontCamera(Boolean frontCamera) {
        this.getEditor().putBoolean(KEY_FRONT_CAMERA, frontCamera);
    }

    //  methods

    private SharedPreferences getPref() {
        //  get application context
        Context context = GlobalApplication.getContext();
        //  get shared preferences
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor() {
        //  return preferences editor
        return this.getPref().edit();
    }
}
