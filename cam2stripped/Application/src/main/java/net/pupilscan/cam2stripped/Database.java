//  Database.java

//  local database for scans, log records, etc.

package net.pupilscan.cam2stripped;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.Settings;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class Database extends SQLiteOpenHelper {

    //  constants

    private static final String DATABASE_NAME = "pupilscan.sqlite3";
    private static final int DATABASE_VERSION = 1;
    private static final String LOG_TABLE = "log";
    private static final String LOG_ID = "log";
    private static final String LOG_CREATED = "created";
    private static final String LOG_BADGE = "badge";
    private static final String LOG_PATIENT = "patient";
    private static final String LOG_TYPE = "type";
    private static final String LOG_INFO = "info";
    private static final SimpleDateFormat gmtFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");

    //  instance variables

    private SQLiteDatabase mDatabase;


    //  singleton

    public Database shared = new Database();

    //  constructor

    public Database() {
        //  call superclass method
        super(GlobalApplication.getContext(), DATABASE_NAME, null, DATABASE_VERSION);
        //  set GMT timezone
        Database.gmtFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

    }

    //  overrides

    @Override
    public void onCreate(SQLiteDatabase db) {
        //  create tables
        String logCreate = "create table " + LOG_TABLE + "(" +
                LOG_ID + " TEXT PRIMARY KEY, " +
                LOG_CREATED + " TEXT NOT NULL, " +
                LOG_BADGE + " TEXT, " +
                LOG_PATIENT + " TEXT, " +
                LOG_TYPE + " TEXT NOT NULL, " +
                LOG_INFO + " TEXT)";
        db.execSQL(logCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //  FUTURE: MIGRATE DATABASE
        //  just recreate the tables
        db.execSQL("DROP TABLE IF EXISTS " + LOG_TABLE);
        //  create the tables
        this.onCreate(db);
    }

    //  methods

    public void addLog(String id, String type, Date created, String info) {
        //  gather insert arguments
        ContentValues contentValues = new ContentValues();
        contentValues.put(LOG_ID, id);
        contentValues.put(LOG_CREATED, gmtFormat.format(created));
        contentValues.put(LOG_TYPE, type);
        //  TODO: BADGE
        //  TODO: PATIENT
        if (info != null)
            contentValues.put(LOG_INFO, info);
        SQLiteDatabase database = this.getWritableDatabase();
        if (database != null)
            database.insert(LOG_TABLE, null, contentValues);
    }

    public Cursor listLogs(Integer limit) {
        Cursor cursor = null;
        SQLiteDatabase database = this.getReadableDatabase();
        if (database != null) {
            String[] columns = new String[]{LOG_ID, LOG_CREATED, LOG_TYPE, LOG_BADGE, LOG_PATIENT, LOG_INFO};
            cursor = database.query(LOG_TABLE, columns, null, null, null, null, LOG_CREATED + " DESC");
            if (cursor != null) {
                cursor.moveToFirst();
            }
        }
        return cursor;
    }
}

/*

  //  MARK: LOG

  struct Log {
    //  instance variables
    var created: String
    var device: String?
    var badge: String?
    var patient: String?
    var type: String
    var info: String?
    //  initializer
    init(_ this: Database, _ record: Row) {
      self.created = record[this.log_created]
      self.badge = record[this.log_badge]
      self.type = record[this.log_type]
      self.info = record[this.log_info]
      self.patient = record[this.log_patient]
    }
  }

  //  MARK: VIDEO

  struct Video {
    //  instance variables
    var id: String
    var partition: String?
    var created: String
    var device: String?
    var badge: String?
    var patient: String?
    var uploaded: Bool
    var processed: Bool
    var meta: String?
    var results: String?
    //  initializer
    init(_ this: Database, _ record: Row) {
      self.id = record[this.video_id]
      self.partition = record[this.video_partition]
      self.created = record[this.video_created]
      self.badge = record[this.video_badge]
      self.patient = record[this.video_patient]
      self.uploaded = record[this.video_uploaded] != 0
      self.processed = record[this.video_processed] != 0
      self.meta = record[this.video_meta]
      self.results = record[this.video_results]
    }
  }

  //  MARK: BADGE

  struct Badge {
    //  instance variables
    var badge: String
    var created: String
    var modified: String
    //  initializer
    init(_ this: Database, _ record: Row) {
      self.badge = record[this.badge_badge]
      self.created = record[this.badge_created]
      self.modified = record[this.badge_modified]
    }
  }

  //  MARK: PILOT

  struct Pilot {
    //  instance variables
    var id: String
    var name: String
    var domains: String?
    var logic: String?
    //  initializer
    init(_ this: Database, _ record: Row) {
      self.id = record[this.pilot_id]
      self.name = record[this.pilot_name]
      self.domains = record[this.pilot_domains]
      self.logic = record[this.pilot_logic]
    }
  }

  //  MARK: INSTANCE VARIABLES

  //  database connection
  private var db: Connection? = nil
  //  tables
  private let logs = Table("logs")
  private let videos = Table("videos")
  private let badges = Table("badges")
  private let pilots = Table("pilots")
  //  fields
  private let log_id = Expression<String>("id")
  private let log_created = Expression<String>("created")
  private let log_badge = Expression<String?>("badge")
  private let log_patient = Expression<String?>("patient")
  private let log_type = Expression<String>("type")
  private let log_info = Expression<String?>("info")
  private let video_id = Expression<String>("id")
  private let video_partition = Expression<String?>("partition")
  private let video_created = Expression<String>("created")
  private let video_badge = Expression<String?>("badge")
  private let video_patient = Expression<String?>("patient")
  private let video_uploaded = Expression<Int>("uploaded")
  private let video_processed = Expression<Int>("processed")
  private let video_meta = Expression<String?>("info")
  private let video_results = Expression<String?>("results")
  private let badge_badge = Expression<String>("badge")
  private let badge_created = Expression<String>("created")
  private let badge_modified = Expression<String>("modified")
  private let pilot_id = Expression<String>("id")
  private let pilot_name = Expression<String>("name")
  private let pilot_domains = Expression<String>("domains")
  private let pilot_logic = Expression<String>("logic")

  //  MARK: SINGLETON

  static var shared: Database = Database()

  //  MARK: INITIALIZER

  init() {
    //  clear instance variables
    self.db = nil
    //  database initialize
    do {
      //  database file
      let fileURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(Const.databaseFile)
      //  connect
      self.db = try Connection(fileURL.path)
      if let db = self.db {
        //  create tables
        try db.run(self.logs.create(ifNotExists: true) { t in
          t.column(log_id, primaryKey: true)
          t.column(log_created)
          t.column(log_badge)
          t.column(log_patient)
          t.column(log_type)
          t.column(log_info)
        })
        try db.run(self.videos.create(ifNotExists: true) { t in
          t.column(video_id, primaryKey: true)
          //t.column(video_partition) //  migration 3
          t.column(video_created)
          t.column(video_badge)
          t.column(video_patient)
          //t.column(video_uploaded)  //  migration 1
          //t.column(video_processed) //  migration 4
          t.column(video_meta)
          //t.column(video_results)   //  migration 2
        })
        try db.run(self.badges.create(ifNotExists: true) { t in
          t.column(badge_badge, primaryKey: true)
          t.column(badge_created)
          t.column(badge_modified)
        })
        try db.run(self.pilots.create(ifNotExists: true) { t in
          t.column(pilot_id, primaryKey: true)
          t.column(pilot_name)
          t.column(pilot_domains)
          t.column(pilot_logic)
        })
        //  migration 1 - add video:uploaded column
        if Settings.shared.database < 1 {
          try db.run(self.videos.addColumn(video_uploaded, defaultValue: 0))
          Settings.shared.database = 1
        }
        //  migration 2 - add video:results column
        if Settings.shared.database < 2 {
          try db.run(self.videos.addColumn(video_results, defaultValue: nil))
          Settings.shared.database = 2
        }
        //  migration 3 - add video:partition column
        if Settings.shared.database < 3 {
          try db.run(self.videos.addColumn(video_partition, defaultValue: nil))
          Settings.shared.database = 3
        }
        //  migration 4 - add video:processed column
        if Settings.shared.database < 4 {
          try db.run(self.videos.addColumn(video_processed, defaultValue: 0))
          Settings.shared.database = 4
        }
      }
    } catch {
      Logging.shared.add(type: "error", info: "database  error=\(error.localizedDescription)")
    }
  }

  //  MARK: METHODS

  public func listLogs(limit: Int? = nil) -> [Log] {
    var result: [Log] = []
    if let db = self.db {
      do {
        let logQuery = self.logs.order(log_created.desc).limit(limit)
        for logItem in try db.prepare(logQuery) {
          result.append(Log(self, logItem))
        }
      } catch {
        Logging.shared.add(type: "error", info: "database  listLogs  error=\(error.localizedDescription)")
      }
    }
    return result
  }

  public func addVideo(id: String, partition: String, created: Date, meta: String? = nil) {
    //  check database connection
    if let db = self.db {
      do {
        //  get other parameters
        let badge: String? = String.nilIfEmpty(Settings.shared.badge)
        let patient: String? = String.nilIfEmpty(Settings.shared.patient)
        let uploaded: Int = 0
        let processed: Int = 0
        //  insert into database
        try db.run(self.videos.insert(video_id <- id, video_partition <- partition, video_created <- created.iso8601, video_badge <- badge, video_patient <- patient, video_uploaded <- uploaded, video_processed <- processed, video_meta <- meta))
      } catch {
        Logging.shared.add(type: "error", info: "database  addVideo  error=\(error.localizedDescription)")
      }
    }
  }

  public func updateVideo(id: String, partition: String? = nil, uploaded: Bool? = nil, processed: Bool? = nil, results: String? = nil) {
    //  check database connection
    if let db = self.db {
      do {
        let videoQuery = self.videos.filter(video_id == id)
        if let partition = partition {
          try db.run(videoQuery.update(video_partition <- partition))
        }
        if let uploaded = uploaded {
          try db.run(videoQuery.update(video_uploaded <- (uploaded ? 1 : 0)))
        }
        if let processed = processed {
          try db.run(videoQuery.update(video_processed <- (processed ? 1 : 0)))
        }
        if let results = results {
          try db.run(videoQuery.update(video_results <- results))
        }
      } catch {
        Logging.shared.add(type: "error", info: "database  updateVideo  error=\(error.localizedDescription)")
      }
    }
  }

  public func countVideos() -> Int {
    var result: Int = 0
    if let db = self.db {
      do {
        result = try db.scalar(self.videos.count)
        //print("Database.countVideos  result=\(result)")
      } catch {
        Logging.shared.add(type: "error", info: "database  countVideos  error=\(error.localizedDescription)")
      }
    }
    return result
  }

  public func listVideos(limit: Int? = nil) -> [Video] {
    var result: [Video] = []
    if let db = self.db {
      do {
        let videoQuery = self.videos.order(video_created.desc).limit(limit)
        for videoItem in try db.prepare(videoQuery) {
          result.append(Video(self, videoItem))
        }
      } catch {
        Logging.shared.add(type: "error", info: "database  listVideos  error=\(error.localizedDescription)")
      }
    }
    return result
  }

  public func getVideo(id: String? = nil, offset: Int? = nil, processed: Bool? = nil) -> Video? {
    var result: Video? = nil
    if let db = self.db {
      do {
        var videoQuery: QueryType? = nil
        if let id = id {
          videoQuery = self.videos.filter(video_id == id)
        }
        if let offset = offset {
          videoQuery = self.videos.order(video_created.desc).limit(1, offset: offset)
        }
        if let processed = processed {
          videoQuery = self.videos.filter(video_processed == (processed ? 1 : 0))
        }
        if let videoQuery = videoQuery {
          for videoItem in try db.prepare(videoQuery) {
            result = Video(self, videoItem)
            break
          }
        }
      } catch {
        Logging.shared.add(type: "error", info: "database  getVideo by id  error=\(error.localizedDescription)")
      }
    }
    return result
  }

  public func deleteVideo(id: String) {
    if let db = self.db {
      do {
        let videoQuery = self.videos.filter(video_id == id)
        try db.run(videoQuery.delete())
      } catch {
        Logging.shared.add(type: "error", info: "database  deleteVideo by id  error=\(error.localizedDescription)")
      }
    }
  }

  public func addBadge(badge: String, created: Date = Date()) {
    //  check database connection
    if let db = self.db {
      do {
        //  see if badge exists
        let badgeQuery = self.badges.filter(badge_badge == badge)
        for _ in try db.prepare(badgeQuery) {
          //  badge exists - update modified time
          try db.run(badgeQuery.update(badge_modified <- created.iso8601))
          return
        }
        //  insert into database
        try db.run(self.badges.insert(badge_badge <- badge, badge_created <- created.iso8601, badge_modified <- created.iso8601))
      } catch {
        Logging.shared.add(type: "error", info: "database  addLog  addVideo=\(error.localizedDescription)")
      }
    }
  }

  public func listBadges(limit: Int? = nil) -> [Badge] {
    var result: [Badge] = []
    if let db = self.db {
      do {
        let badgeQuery = self.badges.order(badge_modified.desc).limit(limit)
        for badgeItem in try db.prepare(badgeQuery) {
          result.append(Badge(self, badgeItem))
        }
      } catch {
        Logging.shared.add(type: "error", info: "database  listBadges  error=\(error.localizedDescription)")
      }
    }
    return result
  }

  public func deletePilots() {
    //  check database connection
    if let db = self.db {
      do {
        try db.run(self.pilots.delete())
      } catch {
        Logging.shared.add(type: "error", info: "database  deletePilots  error=\(error.localizedDescription)")
      }
    }
  }

  public func addPilot(id: String, name: String, domains: String?, logic: String?) {
    //  check database connection
    if let db = self.db {
      do {
        //  see if pilot exists
        let pilotQuery = self.pilots.filter(pilot_id == id)
        for _ in try db.prepare(pilotQuery) {
          //  badge exists - update modified time
          try db.run(pilotQuery.update(pilot_name <- name, pilot_domains <- domains ?? "", pilot_logic <- logic ?? ""))
          return
        }
        //  insert into database
        try db.run(self.pilots.insert(pilot_id <- id, pilot_name <- name, pilot_domains <- domains ?? "", pilot_logic <- logic ?? ""))
      } catch {
        Logging.shared.add(type: "error", info: "database  addPilot  error=\(error.localizedDescription)")
      }
    }
  }

  public func listPilots(limit: Int? = nil) -> [Pilot] {
    var result: [Pilot] = []
    if let db = self.db {
      do {
        let pilotQuery = self.pilots.order(pilot_name.asc)
        for pilotItem in try db.prepare(pilotQuery) {
          result.append(Pilot(self, pilotItem))
        }
      } catch {
        Logging.shared.add(type: "error", info: "database  listPilots  error=\(error.localizedDescription)")
      }
    }
    return result
  }

  public func getPilot(id: String) -> Pilot? {
    var result: Pilot? = nil
    if let db = self.db {
      do {
        let pilotQuery = self.pilots.filter(pilot_id == id)
        for pilotItem in try db.prepare(pilotQuery) {
          result = Pilot(self, pilotItem)
          break
        }
      } catch {
        Logging.shared.add(type: "error", info: "database  getPilot by id  error=\(error.localizedDescription)")
      }
    }
    return result
  }

}
*/