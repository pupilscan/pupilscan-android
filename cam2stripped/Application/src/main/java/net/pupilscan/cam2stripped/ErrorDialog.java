//  ErrorDialog.java

//  creates and presents error dialog with single OK button

package net.pupilscan.cam2stripped;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.FragmentManager;

public class ErrorDialog extends DialogFragment {

    //  constants

    private static final String BUNDLE_MESSAGE = "error_dialog_message";
    private static final String ERROR_DIALOG_TAG = "error_dialog";
    private static final String TAG = "ErrorDialog";

    //  static methods

    public static void show(String message, FragmentManager fragmentManager) {
        //  create ErrorDialog instance
        ErrorDialog errorDialog = new ErrorDialog();
        //  attach arguments to dialog
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_MESSAGE, message);
        errorDialog.setArguments(bundle);
        //  display dialog
        errorDialog.show(fragmentManager, ERROR_DIALOG_TAG);
    }

    //  overrides

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //  need to persist the current activity
        final Activity currentActivity = this.getActivity();
        //  create alert dialog with message and OK action
        return new AlertDialog.Builder(currentActivity)
                .setMessage(this.getArguments().getString(BUNDLE_MESSAGE))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    //  overrides
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        //  dismiss this dialog
                        currentActivity.finish();
                    }
                })
                .create();
    }
}