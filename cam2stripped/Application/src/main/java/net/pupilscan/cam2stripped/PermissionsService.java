//  PermissionsService.java

//  manages permission requirements

package net.pupilscan.cam2stripped;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class PermissionsService {

    //  constants

    private static final int REQUEST_VIDEO_PERMISSIONS = 1;
    private static final String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.INTERNET
    };
    private static final String TAG = "PermissionsService";

    //  static methods

    public static boolean hasPermissionsGranted(Fragment fragment) {
        for (String permission : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(fragment.getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static void requestVideoPermissions(final Fragment fragment) {
        //  see if we can show permission rationale UI
        Boolean canShowPermissionRationale = false;
        for (String permission : PERMISSIONS) {
            if (fragment.shouldShowRequestPermissionRationale(permission)) {
                canShowPermissionRationale = true;
                break;
            }
        }
        if (canShowPermissionRationale)
            //  show confirmation dialog
            ConfirmationDialog.show(
                    fragment.getString(R.string.permission_request),
                    fragment.getFragmentManager(),
                    new ConfirmationDialog.OnConfirmationListener() {
                        //  overrides
                        @Override
                        public void ok() {
                            //  request permissions (again)
                            PermissionsService.requestVideoPermissions(fragment);
                        }
                        @Override
                        public void cancel() {
                            //  ignore; future can display more warnings
                        }
                    }
            );
        else
            //  have Android request the permissions
            fragment.requestPermissions(PERMISSIONS, REQUEST_VIDEO_PERMISSIONS);
    }

    public static Boolean permissionsResult(Fragment fragment, int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult  requestCode=" + requestCode);
        if (requestCode == REQUEST_VIDEO_PERMISSIONS) {
            if (grantResults.length == PERMISSIONS.length) {
                //  loop through grant results
                for (int result : grantResults)
                    //  check permission grant
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        //  display error message - could be tailored to permission in future
                        ErrorDialog.show(fragment.getString(R.string.permission_request), fragment.getChildFragmentManager());
                        //  break since only first ungranted permission should be shown
                        break;
                    }
            } else
                //  display error message since no permissions were granted
                ErrorDialog.show(fragment.getString(R.string.permission_request), fragment.getChildFragmentManager());
            return true;
        } else
            return false;
    }
}
