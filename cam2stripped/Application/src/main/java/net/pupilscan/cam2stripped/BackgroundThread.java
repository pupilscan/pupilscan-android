//  BackgroundThread.java

//  encapsulates a thread and handler for background processing

package net.pupilscan.cam2stripped;

import android.os.Handler;
import android.os.HandlerThread;

class BackgroundThread {

    //  constants

    private static final String THREAD_NAME = "BackgroundThread";

    //  instance variables

    Handler mHandler;
    HandlerThread mHandlerThread;

    //  constructor

    public BackgroundThread() {
        //  start thread
        mHandlerThread = new HandlerThread(THREAD_NAME);
        mHandlerThread.start();
        //  create handler
        mHandler = new Handler(mHandlerThread.getLooper());
    }

    //  methods

    public Handler handler() {
        return mHandler;
    }

    public void stop() {
        //  check to make sure it is running
        if (mHandlerThread != null) {
            //  quit background thread
            mHandlerThread.quitSafely();
            try {
                //  wait for thread to die
                mHandlerThread.join();
                //  clear instance variables
                mHandlerThread = null;
                mHandler = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
