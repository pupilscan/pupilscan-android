//  SystemFragment.java

//  framgent for device info

package net.pupilscan.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StatFs;
import android.os.Bundle;
import android.os.Build;
import android.text.SpannableStringBuilder;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.format.DateUtils;
import android.text.style.UnderlineSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.AbsoluteSizeSpan;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.util.Log;
import android.provider.Settings.Secure;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

public class SystemFragment extends Fragment {

    //  constants

    private static final int HEADER_TOP = 16;
    private static final int HEADER_BOTTOM = 8;
    private static final int MARGIN_BOTTOM = 40;
    private static final int KBYTES = 1024;
    private static final String TAG = "SystemFragment";

    //  instance variables

    private TextView systemTextView;

    //  overrides

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //  call superclass method
        super.onCreateView(inflater, container, savedInstanceState);
        //  convert from XML layout to fragment
        View rootView = inflater.inflate(R.layout.system_fragment, container, false);
        //  find text view
        systemTextView = rootView.findViewById(R.id.text_system);
        //  populate content in text view
        this.setContent();
        return rootView;
    }

    //  methods

    private void setContent() {
        //  check text view
        if (systemTextView != null) {
            //  clear to start
            systemTextView.setText("");
            //  setup Spannable builder
            SpannableStringBuilder builder = new SpannableStringBuilder();
            //  device info
            this.header(builder, "Device");
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            this.keyValue(builder, "Model", model.startsWith(manufacturer) ? this.capitalize(model) : this.capitalize(manufacturer) + " " + model);
            //  noinspection HardwareIds
            @SuppressWarnings({"HardwareIds"})
            String uuid = android.provider.Settings.Secure.getString(GlobalApplication.getContext().getContentResolver(), Secure.ANDROID_ID);
            this.keyValue(builder, "UUID", uuid);
            //  network
            this.header(builder, "Network");
            int connection = R.string.connection_none;
            try {
                //  noinspection ConstantConditions
                @Nullable ConnectivityManager connectivityManager = (ConnectivityManager) this.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivityManager != null) {
                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                    if (networkInfo != null) {
                        if (networkInfo.isConnected()) {
                            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI)
                                connection = R.string.connection_wifi;
                            else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE)
                                connection = R.string.connection_cellular;
                            else
                                connection = R.string.connection_unknown;
                        }
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
                Log.e(TAG, "setContent  unable to get ConnectivityManager instance");
            }
            this.keyValue(builder, "Connection", this.getString(connection));
            if (NetworkService.ipAddress != null)
                this.keyValue(builder, "IP Address", NetworkService.ipAddress);
            if (NetworkService.publicIP != null)
                this.keyValue(builder, "Public IP", NetworkService.publicIP);
            //  app
            this.header(builder, "App");
            try {
                //  noinspection ConstantConditions
                @Nullable PackageManager packageManager = this.getContext().getPackageManager();
                if (packageManager != null) {
                    try {
                        PackageInfo packageInfo = packageManager.getPackageInfo(this.getContext().getPackageName(), 0);
                        if (packageInfo != null) {
                            this.keyValue(builder, "Version", packageInfo.versionName);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
                                this.keyValue(builder, "Build", String.valueOf(packageInfo.getLongVersionCode()));
                            else
                                this.keyValue(builder, "Build", String.valueOf(packageInfo.versionCode));
                            if (packageInfo.lastUpdateTime > 0)
                                this.keyValue(builder, "Built", DateUtils.formatDateTime(this.getContext(), packageInfo.lastUpdateTime, DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE |
                                        DateUtils.FORMAT_SHOW_YEAR));
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                        Log.e(TAG, "setContent  unable to get package info");
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
                Log.e(TAG, "setContent  unable to get PackageManager instance");
            }
            //  space usage
            this.header(builder, "Space Usage");
            this.keyValue(builder, "System Free", this.freeSpace() + " of " + this.totalSpace());
            this.keyValue(builder, "App Usage", this.appSpace());
            //  queued upload files
            this.header(builder, "Queued Upload Files");
            //  TODO: queued files list
            //  recent activity
            this.header(builder, "Recent Activity");
            //  TODO: recent activity (log records)
            //  trailing space
            this.space(builder, MARGIN_BOTTOM);
            //  set text
            systemTextView.setText(builder);
        }
    }

    private void space(@NonNull SpannableStringBuilder builder, int size) {
        SpannableString spannableString = new SpannableString("\n");
        spannableString.setSpan(new AbsoluteSizeSpan(size, true), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spannableString);
    }

    private void header(@NonNull SpannableStringBuilder builder, @NonNull String text) {
        //  space
        this.space(builder, HEADER_TOP);
        //  header
        SpannableString spannableString = new SpannableString(text + "\n");
        spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(Color.LTGRAY), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spannableString);
        //  space
        this.space(builder, HEADER_BOTTOM);
    }

    private void keyValue(@NonNull SpannableStringBuilder builder, @NonNull String key, @NonNull String value) {
        SpannableString spannableString = new SpannableString(key + ": ");
        spannableString.setSpan(new ForegroundColorSpan(Color.LTGRAY), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spannableString);
        spannableString = new SpannableString(value + "\n");
        spannableString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spannableString);
    }

    private String capitalize(@NonNull String text) {
        if (!TextUtils.isEmpty(text)) {
            boolean capitalizeNext = true;
            StringBuilder builder = new StringBuilder();
            for (char ch : text.toCharArray()) {
                if (capitalizeNext && Character.isLetter(ch)) {
                    ch = Character.toUpperCase(ch);
                    capitalizeNext = false;
                } else if (Character.isWhitespace(ch))
                    capitalizeNext = true;
                builder.append(ch);
            }
            return builder.toString();
        } else
            return text;
    }

    private ArrayList<String> getPaths() {
        ArrayList<String> paths = new ArrayList<String>();
        try {
            //  noinspection ConstantConditions
            @Nullable File[] directories = this.getContext().getExternalFilesDirs(null);
            if (directories != null)
                for (File directory : directories)
                    paths.add(directory.getAbsolutePath());
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.e(TAG, "getPaths  unable to get external file directories");
        }
        return paths;
    }

    private long totalSpace(@NonNull String path) {
        StatFs statFs = new StatFs(path);
        return (statFs.getBlockCountLong() * statFs.getBlockSizeLong());
    }

    private String totalSpace() {
        long result = 0;
        ArrayList<String> paths = this.getPaths();
        if (paths != null)
            for (String path : paths)
                result += this.totalSpace(path);
        return displayBytes(result);
    }

    private long freeSpace(@NonNull String path) {
        StatFs statFs = new StatFs(path);
        return (statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong());
    }

    private String freeSpace() {
        long result = 0;
        ArrayList<String> paths = this.getPaths();
        if (paths != null)
            for (String path : paths)
                result += this.freeSpace(path);
        return displayBytes(result);
    }

    private long usedSpace(@NonNull String path) {
        long total = totalSpace(path);
        long free = freeSpace(path);
        return total - free;
    }

    private String usedSpace() {
        long result = 0;
        ArrayList<String> paths = this.getPaths();
        if (paths != null)
            for (String path : paths)
                result += this.usedSpace(path);
        return displayBytes(result);
    }

    private String appSpace() {
        File directory = null;
        try {
            //  noinspection ConstantConditions
            directory = this.getContext().getFilesDir();
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.e(TAG, "appSpace  unable to get file directory");
        }
        //Log.d(TAG, "appSpace  directory=" + directory);
        return displayBytes((directory != null) ? this.usedSpace(directory.getAbsolutePath()) : 0);
    }

    private String displayBytes(long bytes) {
        if (bytes >= KBYTES) {
            int power = (int) (Math.log(bytes) / Math.log(KBYTES));
            return String.format(Locale.ENGLISH, "%.1f %sB", bytes / Math.pow(KBYTES, power), "KMGTPE".charAt(power - 1));
        } else
            return bytes + " B";
    }
}