package net.pupilscan.app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class OverlayView extends View {

    //  instance variables

    private final Object lock = new Object();

    //  constructor

    public OverlayView(Context context, AttributeSet attrs) {
        //  call superclass constructor
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //  call superclass method
        super.onDraw(canvas);
        //  lock
        synchronized (lock) {
            //  get size
            Rect rect = new Rect(0, 0, this.getWidth(), this.getHeight());
            //  set paint
//            Paint paint = new Paint();
//            paint.setColor(Color.GREEN);
//            paint.setAntiAlias(true);
//            paint.setStrokeWidth(5);
//            paint.setStyle(Paint.Style.STROKE);
//            paint.setStrokeJoin(Paint.Join.ROUND);
//            paint.setStrokeCap(Paint.Cap.ROUND);
//            //  draw test line
//            canvas.drawLine(0, 0, rect.right, rect.bottom, paint);
        }
    }
}
