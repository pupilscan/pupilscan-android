//  BaseActivity.java

//  common activity

package net.pupilscan.app;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    //  constants

    private static final String TAG = "BaseActivity";

    //  overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  call superclass method
        super.onCreate(savedInstanceState);
        //  hide actionbar
        this.getSupportActionBar().hide();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!PermissionsUtility.permissionsResult(this, requestCode, permissions, grantResults))
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}