//  NetworkService.java

//  retrieve local and public IP addresses

package net.pupilscan.app;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

public class NetworkService {

    //  constants

    private static final String IPIFY = "https://api.ipify.org";

    //  static variables

    public static String ipAddress = null;
    public static String publicIP = null;
    public static String TAG = "NetworkService";

    //  static methods

    public static void getIpAddress() {
        //  get global context
        Context context = GlobalApplication.getContext();
        //  get WiFi maanager
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            //  retrieve IP address in numeric format
            int ipInteger = wifiManager.getDhcpInfo().ipAddress;
            if (ipInteger > 0) {
                //  convert from IP number to Internet address
                InetAddress inetAddress = toInetAddress(ipInteger);
                if (inetAddress != null) {
                    //  finally convert to string
                    ipAddress = inetAddress.toString().replace("/", "");
                    Log.d(TAG, "getIpAddress  ipAddress=" + ipAddress);
                }
            }
        }
    }

    public static void getPublicIP() {
        //  run in another thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                //  trap errors
                try {
                    //  get localhost
                    InetAddress localhost = InetAddress.getLocalHost();
                    if (localhost != null) {
                        //  having a localhist implies that we can access a website or web service
                        URL urlIpify = new URL(IPIFY);
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlIpify.openStream()));
                        //  retrieve public IP
                        publicIP = bufferedReader.readLine().trim();
                        Log.d(TAG, "getIpAddress  publicIP=" + publicIP);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //  clear public IP
                    publicIP = null;
                }
            }
        }).start();
    }

    private static InetAddress toInetAddress(int address) {
        //  convert from int to bytes
        byte[] bytes = {
                (byte) (0xff & address),
                (byte) (0xff & (address >> 8)),
                (byte) (0xff & (address >> 16)),
                (byte) (0xff & (address >> 24)) };
        try {
            //  convert to internet address object
            return InetAddress.getByAddress(bytes);
        } catch (UnknownHostException e) {
            //  unable to convert
            e.printStackTrace();
        }
        return null;
    }
}
