//  NavBarFragment.java

//  framgent for top navigation of badge/barcode/face activities

package net.pupilscan.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class NavBarFragment extends Fragment implements View.OnClickListener {

    //  constants

    private static final String TAG = "NavBarFragment";

    //  widgets

    private ImageButton buttonMenu;

    //  overrides

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //  call superclass method
        super.onCreateView(inflater, container, savedInstanceState);
        //  convert from XML layout to fragment
        View rootView = inflater.inflate(R.layout.navbar_fragment, container, false);
        //  declare views
        this.buttonMenu = rootView.findViewById(R.id.button_menu);
        //  add listeners
        this.buttonMenu.setOnClickListener(this);
        return rootView;
    }

    //  OnClickListener

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_menu:
                //  activate tab controller
                Log.d(TAG, "onClick  menu");
                Intent intent = new Intent(this.getActivity(), TabContainerActivity.class);
                this.startActivity(intent);
                //  animate
                this.getActivity().overridePendingTransition(R.anim.animation_enter, R.anim.animation_fadeout);
                break;
        }
    }
}