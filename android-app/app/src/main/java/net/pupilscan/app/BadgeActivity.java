//  BadgeActivity.java

//  type in or scan clinician badge

package net.pupilscan.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.TextureView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

import java.util.List;

public class BadgeActivity extends CameraActivity implements View.OnClickListener, TextureView.SurfaceTextureListener {

    //  constants

    private static final String TAG = "BadgeActivity";
    private static final String MESSAGE_BADGE = "badge";
    private static final String MESSAGE_EXTRA = "extra";
    private static final String BADGE_PREFIX = "ID_";

    //  instance views and widgets

    private Button buttonBadge;
    private ImageButton buttonCamera;
    private ImageButton buttonNFC;
    private EditText editBadge;

    //  instance variables

    private FirebaseVisionBarcodeDetector barcodeDetector;

    //  listener & receiver classes (not interfaces)

    private TextWatcher textWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //  refresh interface
            updateUI();
        }
    };
    private EditText.OnEditorActionListener editorActionListener = new EditText.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                //  pretend button hit
                buttonBadge.performClick();
                //  done
                return true;
            }
            return false;
        }
    };
    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ((intent.getAction() == MESSAGE_BADGE) && intent.hasExtra(MESSAGE_EXTRA)) {
                String barcode = intent.getStringExtra(MESSAGE_EXTRA);
                if (barcode != null)
                    editBadge.setText(barcode);
            }
        }
    };

    //  overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  call superclass method
        super.onCreate(savedInstanceState);
        //  set content view using the container layout
        this.setContentView(R.layout.badge_activity);
        //  barcode detector
        this.barcodeDetector = FirebaseVision.getInstance().getVisionBarcodeDetector();
        //  check permissions
        PermissionsUtility.checkPermissions(this);
        //  assign instance
        this.textureView = this.findViewById(R.id.texture_badge);
        this.editBadge = this.findViewById(R.id.edit_badge);
        this.buttonBadge = this.findViewById(R.id.button_badge);
        this.buttonCamera = this.findViewById(R.id.button_camera);
        this.buttonNFC = this.findViewById(R.id.button_nfc);
        //  attach listeners
        this.buttonBadge.setOnClickListener(this);
        this.buttonCamera.setOnClickListener(this);
        this.buttonNFC.setOnClickListener(this);
        this.editBadge.addTextChangedListener(this.textWatcher);
        this.editBadge.setImeActionLabel(this.getString(R.string.badge_enter), KeyEvent.KEYCODE_ENTER);
        this.editBadge.setOnEditorActionListener(this.editorActionListener);
        //  focus
        this.editBadge.requestFocus();
        //  set button states
        this.updateUI();
    }

    @Override
    public void onResume() {
        //  call superclass method
        super.onResume();
        //  set as receiver
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_BADGE);
        this.registerReceiver(this.messageReceiver, intentFilter);
        //  default badge
        String badge = Settings.shared.getBadge();
        if (badge != null)
            this.editBadge.setText(badge);
    }

    @Override
    public void onPause() {
        //  stop messaging
        this.unregisterReceiver(this.messageReceiver);
        //  remember badge
        Settings.shared.setBadge(this.editBadge.getText().toString());
        //  call superclass method
        super.onPause();
    }

    //  instance methods

    private void updateUI() {
        //  camera
        this.buttonCamera.setEnabled(((this.cameraList != null) && (this.cameraList.length > 0)));
        //  NFC support
        //  TODO: ADD NFC SUPPORT
        this.buttonNFC.setEnabled(false);
        //  set button text
        String badge = this.editBadge.getText().toString().trim();
        this.buttonBadge.setText(badge.isEmpty() ? R.string.badge_skip : R.string.badge_continue);
    }

    @SuppressWarnings("MissingPermission")
    @Override
    protected void openCamera() {
        //  call superclass method
        super.openCamera();
        //  update user interface
        this.updateUI();
    }

    @Override
    protected void processBitmap(@NonNull Bitmap bitmap) {
        //  check arguments
        if ((bitmap != null) && (this.barcodeDetector != null)) {
            //  convert from bitmap to FirebaseVisionImage
            FirebaseVisionImage visionImage = FirebaseVisionImage.fromBitmap(bitmap);
            if (visionImage != null) {
                //  start barcode detection (performed in background)
                barcodeDetector.detectInImage(visionImage)
                        .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                            @Override
                            public void onSuccess(List<FirebaseVisionBarcode> firebaseVisionBarcodes) {
                                //Log.d(TAG, "processBitmap  OnSuccess  firebaseVisionBarcodes=" + firebaseVisionBarcodes);
                                if (!firebaseVisionBarcodes.isEmpty())
                                    for (FirebaseVisionBarcode barcode : firebaseVisionBarcodes) {
                                        //  populate edit text with barcode
                                        Intent barcodeIntent = new Intent();
                                        barcodeIntent.setAction(MESSAGE_BADGE);
                                        barcodeIntent.putExtra(MESSAGE_EXTRA, barcode.getRawValue());
                                        BadgeActivity.this.sendBroadcast(barcodeIntent);
                                        Log.d(TAG, "processBitmap  barcode=" + barcode.getRawValue());
                                    }
                                //  release semaphore
                                BadgeActivity.super.processBitmap(bitmap);
                            }
                        })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e(TAG, "processBitmap  OnFailureListener  e=" + e.getLocalizedMessage());
                                        //  release semaphore
                                        BadgeActivity.super.processBitmap(bitmap);
                                    }
                                });
            } else
                //  release semaphore
                super.processBitmap(bitmap);
        } else
            //  release semaphore
            super.processBitmap(bitmap);
    }

    //  OnClickListener

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_badge:
                //  remember badge
                String badge = this.editBadge.getText().toString().trim();
                if ((badge == null) || badge.isEmpty()) {
                    //  generate random badge number
                    badge = BADGE_PREFIX + System.currentTimeMillis() / 1000;
                    //  display immediately
                    this.editBadge.setText(badge);
                }
                //  persist badge
                Settings.shared.setBadge(badge);
                //  blur input
                this.editBadge.clearFocus();
                //  continue to barcode activity
                Intent intent = new Intent(this, BarcodeActivity.class);
                this.startActivity(intent);
                //  animate
                this.overridePendingTransition(R.anim.animation_enter, R.anim.animation_fadeout);
                break;
            case R.id.button_camera:
                if ((this.cameraList != null) && (this.cameraList.length > 1)) {
                    //  close current camera
                    this.closeCamera();
                    //  next camera
                    this.cameraSelected = (this.cameraSelected + 1) % this.cameraList.length;
                    //  open camera
                    this.openCamera();
                }
                break;
            case R.id.button_nfc:
                Log.d(TAG, "onClick  NFC clicked");
                break;
        }
    }
}
