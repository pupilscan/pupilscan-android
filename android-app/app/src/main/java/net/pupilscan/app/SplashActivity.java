//  SplashActivity.java

//  display splash screen

package net.pupilscan.app;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    //  overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  call superclass method
        super.onCreate(savedInstanceState);
        //  start real main activity
        this.startActivity(new Intent(SplashActivity.this, BadgeActivity.class));
        //  close splash activity
        this.finish();
    }
}
