//  AboutFragment.java

//  framgent for about screen

package net.pupilscan.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class AboutFragment extends Fragment {

    //  constants

    private static final String TAG = "AboutFragment";

    //  instance variables

    //private ...

    //  overrides

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //  call superclass method
        super.onCreateView(inflater, container, savedInstanceState);
        //  convert from XML layout to fragment
        View rootView = inflater.inflate(R.layout.about_fragment, container, false);
        //  declare views
        //systemTextView = rootView.findViewById(R.id.text_system);
        return rootView;
    }
}