package net.pupilscan.app;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import com.google.android.gms.vision.CameraSource;

import java.util.ArrayList;
import java.util.List;

public class OverlayCanvas extends View {

    //  instance variables

    private final Object lock = new Object();
    private int previewWidth;
    private float widthScaleFactor = 1.0f;
    private int previewHeight;
    private float heightScaleFactor = 1.0f;
    private boolean mirror = false;
    private final List<Graphic> graphics = new ArrayList<>();

    //  abstract class for drawing primitives

    public abstract static class Graphic {

        //  instance variables

        private OverlayCanvas overlay;



        public Graphic(OverlayCanvas overlay) {
            this.overlay = overlay;
        }

        /**
         * Draw the graphic on the supplied canvas. Drawing should use the following methods to convert
         * to view coordinates for the graphics that are drawn:
         *
         * <ol>
         *   <li>{@link Graphic#scaleX(float)} and {@link Graphic#scaleY(float)} adjust the size of the
         *       supplied value from the preview scale to the view scale.
         *   <li>{@link Graphic#translateX(float)} and {@link Graphic#translateY(float)} adjust the
         *       coordinate from the preview's coordinate system to the view coordinate system.
         * </ol>
         *
         * @param canvas drawing canvas
         */
        public abstract void draw(Canvas canvas);

        /**
         * Adjusts a horizontal value of the supplied value from the preview scale to the view scale.
         */
        public float scaleX(float horizontal) {
            return horizontal * overlay.widthScaleFactor;
        }

        /** Adjusts a vertical value of the supplied value from the preview scale to the view scale. */
        public float scaleY(float vertical) {
            return vertical * overlay.heightScaleFactor;
        }

        /** Returns the application context of the app. */
        public Context getApplicationContext() {
            return overlay.getContext().getApplicationContext();
        }

        /**
         * Adjusts the x coordinate from the preview's coordinate system to the view coordinate system.
         */
        public float translateX(float x) {
            return overlay.mirror ? overlay.getWidth() - scaleX(x) : scaleX(x);
        }

        /**
         * Adjusts the y coordinate from the preview's coordinate system to the view coordinate system.
         */
        public float translateY(float y) {
            return scaleY(y);
        }

        public void postInvalidate() {
            overlay.postInvalidate();
        }
    }

    public OverlayCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void clear() {
        synchronized (lock) {
            this.graphics.clear();
        }
        this.postInvalidate();
    }

    public void add(Graphic graphic) {
        synchronized (lock) {
            this.graphics.add(graphic);
        }
    }

    public void remove(Graphic graphic) {
        synchronized (lock) {
            this.graphics.remove(graphic);
        }
        this.postInvalidate();
    }

    public void setCameraInfo(int previewWidth, int previewHeight, boolean mirror) {
        synchronized (lock) {
            this.previewWidth = previewWidth;
            this.previewHeight = previewHeight;
            this.mirror = mirror;
        }
        this.postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //  call superclass method
        super.onDraw(canvas);
        //  lock
        synchronized (lock) {
            //  set scale factors
            if ((previewWidth != 0) && (previewHeight != 0)) {
                widthScaleFactor = (float) canvas.getWidth() / (float) previewWidth;
                heightScaleFactor = (float) canvas.getHeight() / (float) previewHeight;
            }
            //  draw graphic objects
            for (Graphic graphic : graphics) {
                graphic.draw(canvas);
            }
        }
    }
}
