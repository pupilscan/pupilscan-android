//  PermissionsUtility.java

//  manages permission requirements

package net.pupilscan.app;

import android.Manifest;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class PermissionsUtility {

    //  constants

    private static final int REQUEST_PERMISSIONS = 1;
    private static final String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            //Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String TAG = "PermissionsUtility";

    //  static methods

//    public static boolean hasPermissionsGranted(AppCompatActivity activity) {
//        //  for all permissions
//        for (String permission : PERMISSIONS)
//            //  see if permission was granted
//            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
//                return false;
//        return true;
//    }

//    public static void requestPermissions(Fragment fragment) {
//        //  see if we can show permission rationale UI
//        Boolean canShowPermissionRationale = false;
//        for (String permission : PERMISSIONS)
//            if (fragment.shouldShowRequestPermissionRationale(permission)) {
//                canShowPermissionRationale = true;
//                break;
//            }
//        if (canShowPermissionRationale)
//            //  show confirmation dialog
//            ConfirmationDialog.show(
//                    fragment.getString(R.string.permission_request),
//                    fragment.getFragmentManager(),
//                    new ConfirmationDialog.OnConfirmationListener() {
//                        //  overrides
//                        @Override
//                        public void ok() {
//                            //  request permissions (again)
//                            PermissionsUtility.requestPermissions(fragment);
//                        }
//                        @Override
//                        public void cancel() {
//                            //  ignore; future can display more warnings
//                        }
//                    }
//            );
//        else
//            //  have Android request the permissions
//            fragment.requestPermissions(PERMISSIONS, REQUEST_PERMISSIONS);
//    }

    public static void requestPermissions(AppCompatActivity activity) {
        //  see if we can show permission rationale UI
        Boolean canShowPermissionRationale = false;
        for (String permission : PERMISSIONS)
            if (activity.shouldShowRequestPermissionRationale(permission)) {
                canShowPermissionRationale = true;
                break;
            }
        if (canShowPermissionRationale)
            //  show confirmation dialog
            ConfirmationDialog.show(
                    activity.getString(R.string.permission_request),
                    activity.getSupportFragmentManager(),
                    new ConfirmationDialog.OnConfirmationListener() {
                        //  overrides
                        @Override
                        public void ok() {
                            //  request permissions (again)
                            PermissionsUtility.requestPermissions(activity);
                        }
                        @Override
                        public void cancel() {
                            //  ignore; future can display more warnings
                        }
                    }
            );
        else
            //  have Android request the permissions
            activity.requestPermissions(PERMISSIONS, REQUEST_PERMISSIONS);
    }

    public static void checkPermissions(AppCompatActivity activity) {
        //  for all permissions
        for (String permission : PERMISSIONS)
            //  see if permission was granted
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                //  see if we can show permission rationale UI
               if (activity.shouldShowRequestPermissionRationale(permission))
                    //  show confirmation dialog
                    ConfirmationDialog.show(
                            activity.getString(R.string.permission_request),
                            activity.getSupportFragmentManager(),
                            new ConfirmationDialog.OnConfirmationListener() {
                                //  overrides
                                @Override
                                public void ok() {
                                    //  request permissions (again)
                                    PermissionsUtility.requestPermissions(activity);
                                }
                                @Override
                                public void cancel() {
                                    //  ignore; future can display more warnings
                                }
                            }
                    );
                else
                    //  have Android request the permissions
                    activity.requestPermissions(PERMISSIONS, REQUEST_PERMISSIONS);
            }
    }

    public static boolean verifyPermissions(int[] grantResults) {
        //  at least one result must be checked.
        if (grantResults.length < 1)
            return false;
        //  verify each required permission
        for (int result : grantResults)
            if (result != PackageManager.PERMISSION_GRANTED)
                return false;
        return true;
    }

    public static Boolean permissionsResult(AppCompatActivity activity, int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult  requestCode=" + requestCode);
        //  check code
        if (requestCode == REQUEST_PERMISSIONS) {
            //  check permissions array
            if (permissions.length > 0)
                //  validate grant results
                if (!verifyPermissions(grantResults))
                    //  display error message - could be tailored to permission in future
                    ErrorDialog.show(activity.getString(R.string.permission_request), activity.getSupportFragmentManager());
            //  indicate permissions handled
            return true;
        } else
            //  wrong permission code
            return false;
    }
}
