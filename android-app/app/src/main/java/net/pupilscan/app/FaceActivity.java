//  BarcodeActivity.java

//  type in or scan patient barcode

package net.pupilscan.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceContour;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class FaceActivity extends CameraActivity implements View.OnClickListener, TextureView.SurfaceTextureListener {

    //  constants

    private static final String TAG = "FaceActivity";
    private static final String MESSAGE_FACE = "face";
    private static final String MESSAGE_EXTRA = "extra";
    private static final int STATE_READY = 0;
    private static final int STATE_RECORDING = 1;
    private static final int STATE_COMPLETED = 2;
    private static final long TIMER_DURATION = 6000;
    private static final long TIMER_INTERVAL = 500;
    private static final int ENCODING_BITRATE = 10000000;
    private static final int VIDEO_FRAMERATE = 30;
    private static final float SPEECH_RATE = 1.4f;

    //  instance views and widgets

    private TextView textFace;
    private ButtonWidget buttonScan;
    private String uuid;
    private Button buttonCancel;
    private OverlayView viewOverlay;

    //  static variables

    private FirebaseVisionFaceDetector faceDetector;

    //  instance variables

    private int state;
    private CountDownTimer timer;
    private TextToSpeech textToSpeech;
    private MediaRecorder mediaRecorder;

    //  listener & receiver classes (not interfaces)

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ((intent.getAction() == MESSAGE_FACE) && intent.hasExtra(MESSAGE_EXTRA)) {
//                String barcode = intent.getStringExtra(MESSAGE_EXTRA);
//                if (barcode != null) {
//                    //  store patient barcode
//                    Settings.shared.setPatient(barcode);
//                    //  continue to face activity
//                    Log.d(TAG, "onReceive  continue to face activity");
//                    //  TODO:
//                }
            }
        }
    };

    //  overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  call superclass method
        super.onCreate(savedInstanceState);
        //  set content view using the container layout
        this.setContentView(R.layout.face_activity);
        //  unique video identifier
        this.uuid = UUID.randomUUID().toString();
        //  initialize instance variables
        this.state = STATE_READY;
        //  face detector
        FirebaseVisionFaceDetectorOptions faceDetectorOptions = new FirebaseVisionFaceDetectorOptions.Builder()
                .setPerformanceMode(FirebaseVisionFaceDetectorOptions.FAST)
                .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
                .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                .setContourMode(FirebaseVisionFaceDetectorOptions.ALL_CONTOURS)
                .setMinFaceSize(0.15f)
                .enableTracking()
                .build();
        //faceDetectorOptions.isTrackingEnabled()
        this.faceDetector = FirebaseVision.getInstance().getVisionFaceDetector(faceDetectorOptions);
        //  text-to-speech
        this.textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.US);
                    textToSpeech.setSpeechRate(SPEECH_RATE);
                }
            }
        });
        //  assign instance
        this.textureView = this.findViewById(R.id.texture_face);
        this.viewOverlay = this.findViewById(R.id.view_overlay);
        this.textFace = this.findViewById(R.id.text_face);
        this.buttonScan = this.findViewById(R.id.button_scan);
        this.buttonCancel = this.findViewById(R.id.button_cancel);
        //  attach listeners
        this.buttonScan.setOnClickListener(this);
        this.buttonCancel.setOnClickListener(this);
        //  set button states
        this.updateUI();
    }

    @Override
    public void onResume() {
        //  call superclass method
        super.onResume();
        //  set as receiver
        IntentFilter barcodeIntentFilter = new IntentFilter();
        barcodeIntentFilter.addAction(MESSAGE_FACE);
        this.registerReceiver(this.messageReceiver, barcodeIntentFilter);
    }

    @Override
    public void onPause() {
        //  stop if recording
        if (this.state == STATE_RECORDING)
            this.abortScan();
        //  stop text-to-speech
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        //  stop messaging
        this.unregisterReceiver(this.messageReceiver);
        //  call superclass method
        super.onPause();
    }

    @Override
    protected void openCamera() {
        //  call superclass method
        super.openCamera();
        //  instantiate and setup media recorder
        this.mediaRecorder = new MediaRecorder();
    }

    @Override
    protected void closeCamera() {
        //  stop media recording
        if (this.mediaRecorder != null) {
            this.mediaRecorder.release();
            this.mediaRecorder = null;
        }
        //  call superclass method
        super.closeCamera();
    }

    //  instance methods

    private void updateUI() {
        //  scan button
        switch (this.state) {
            case STATE_READY:
                this.textFace.setText(R.string.face_text);
                this.buttonScan.setText(R.string.face_scan);
                this.buttonScan.setDanger(false);
                this.buttonScan.refreshDrawableState();
                this.buttonCancel.setEnabled(true);
                break;
            case STATE_RECORDING:
                this.textFace.setText(R.string.face_text);
                this.buttonScan.setText(R.string.face_abort);
                this.buttonScan.setDanger(true);
                this.buttonScan.refreshDrawableState();
                this.buttonCancel.setEnabled(false);
                break;
            case STATE_COMPLETED:
                this.textFace.setText(R.string.face_accepted);
                this.buttonScan.setText(R.string.face_another);
                this.buttonScan.setDanger(false);
                this.buttonScan.refreshDrawableState();
                this.buttonCancel.setEnabled(true);
                break;
        }
    }

    private void startScan() {
        Log.d(TAG, "startScan");
        //  set state
        this.state = STATE_RECORDING;
        //  stop current session
        this.closeSession();
        //  setup media recorder
        try {
            this.mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
            this.mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        //  media output file
        Log.d(TAG, "startScan  videoPath=" + FileUtility.videoPath(uuid + ".mp4"));
        this.mediaRecorder.setOutputFile(FileUtility.videoPath(uuid + ".mp4"));
        this.mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        //  video encoding specs
        this.mediaRecorder.setVideoEncodingBitRate(ENCODING_BITRATE);
        this.mediaRecorder.setVideoFrameRate(VIDEO_FRAMERATE);
        this.mediaRecorder.setVideoSize(this.cameraSize.getWidth(), this.cameraSize.getHeight());
        this.mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        this.mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        this.mediaRecorder.setOrientationHint(this.cameraOrientation);
        try {
            this.mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "startScan  unable to prepare media recorder");
        }
        //  start new session
        this.startSession();
        //  start timer
        this.timer = new CountDownTimer(TIMER_DURATION, TIMER_INTERVAL) {
            public void onTick(long millisUntilFinished) {
                //  get interval
                long interval = (millisUntilFinished + (TIMER_INTERVAL / 4)) / TIMER_INTERVAL;
                Log.d(TAG, "onTick  interval=" + interval);
                //  check for torch trigger
                boolean desiredTorch = (interval == 10) || (interval == 11);
                if (isTorchOn != desiredTorch) {
                    //  set torch
                    isTorchOn = desiredTorch;
                    //  update camera session
                    updateSession();
                }
                //  text-to-speech
                if (Settings.shared.getAudio() && (textToSpeech != null) && (interval <= 10) && (interval % 2 == 0)) {
                    String speech = (interval > 0) ? String.valueOf(interval / 2) : getString(R.string.face_completed);
                    textToSpeech.speak(speech, TextToSpeech.QUEUE_ADD, null);
                }
            }

            public void onFinish() {
                //  forget timer
                timer = null;
                //  change state
                state = STATE_COMPLETED;
                //  stop recording session
                if (mediaRecorder != null) {
                    mediaRecorder.stop();
                    mediaRecorder.reset();
                }
                //  start preview session immediately
                startSession();
                //  update user interface
                updateUI();
            }
        };
        //  update user interface
        this.updateUI();
    }

    @Override
    protected Surface supplementalSurface() {
        return ((this.mediaRecorder != null) && (this.state == STATE_RECORDING)) ? this.mediaRecorder.getSurface() : super.supplementalSurface();
    }

    @Override
    protected void sessionCallback() {
        Log.d(TAG, "sessionCallback  mediaRecorder=" + this.mediaRecorder + "  state=" + this.state);
        //  check media recorder and state
        if ((this.mediaRecorder != null) && (this.state == STATE_RECORDING)) {
            //  start media recording
            this.mediaRecorder.start();
            //  start timer
            if (this.timer != null)
                this.timer.start();
        }
    }

    private void abortScan() {
        Log.d(TAG, "abortScan");
        //  stop time
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
        //  stop text-to-speech
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        //  turn off torch
        if (this.isTorchOn) {
            this.isTorchOn = false;
            this.updateSession();
        }
        //  set state
        this.state = STATE_READY;
        //  update user interface
        this.updateUI();
    }

    private void stopScan() {
        Log.d(TAG, "abortScan");
        //  set state
        this.state = STATE_READY;
        //  update user interface
        this.updateUI();
    }

    @Override
    protected void processBitmap(@NonNull Bitmap bitmap) {
        //  check arguments
        if ((bitmap != null) && (this.faceDetector != null)) {
            //  reduce bitmap size
            Bitmap smallerBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 2, bitmap.getHeight() / 2, true);
            FirebaseVisionImage visionImage = FirebaseVisionImage.fromBitmap(smallerBitmap);
            if (visionImage != null) {
                //  start face detection (performed in background)
                Log.d(TAG, "processBitmap  calling faceDetector  bitmap size=" + bitmap.getWidth() + "," + bitmap.getHeight());
                faceDetector.detectInImage(visionImage)
                        .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionFace>>() {
                            @Override
                            public void onSuccess(List<FirebaseVisionFace> firebaseVisionFaces) {
                                Log.d(TAG, "processBitmap  OnSuccess  firebaseVisionFaces=" + firebaseVisionFaces);
                                if (!firebaseVisionFaces.isEmpty())
                                    for (FirebaseVisionFace face : firebaseVisionFaces) {
                                        FirebaseVisionFaceContour contour = face.getContour(FirebaseVisionFaceContour.ALL_POINTS);
                                        for (FirebaseVisionPoint point : contour.getPoints()) {
                                            Log.d(TAG, "processBitmap  contour point=" + point);
                                            break;
//                                            float px = translateX(point.getX());
//                                            float py = translateY(point.getY());
//                                            canvas.drawCircle(px, py, FACE_POSITION_RADIUS, facePositionPaint);
                                        }
                                        FirebaseVisionFaceLandmark rightEye = face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_EYE);
                                        if (rightEye != null) {
                                            FirebaseVisionPoint rightPoint = rightEye.getPosition();
                                            if (rightPoint != null) {
                                                Log.d(TAG, "processBitmap  rightPoint=" + rightPoint);
                                            }
                                        }
                                        FirebaseVisionFaceLandmark leftEye = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EYE);
                                        if (leftEye != null) {

                                        }
                                        //FaceContourGraphic faceGraphic = new FaceContourGraphic(graphicOverlay, face);

                                        //
//                                        Intent faceIntent = new Intent();
//                                        faceIntent.setAction(MESSAGE_FACE);
//                                        //faceIntent.putExtra(MESSAGE_EXTRA, barcode.getRawValue());
//                                        FaceActivity.this.sendBroadcast(faceIntent);
//                                        Log.d(TAG, "processBitmap  barcode=" + face.getRawValue());
                                    }
                                //  release semaphore
                                FaceActivity.super.processBitmap(bitmap);
                            }
                        })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e(TAG, "processBitmap  OnFailureListener  e=" + e.getLocalizedMessage());
                                        //  release semaphore
                                        FaceActivity.super.processBitmap(bitmap);
                                    }
                                });
            } else
                //  release semaphore
                super.processBitmap(bitmap);
        } else
            //  release semaphore
            super.processBitmap(bitmap);
    }

    //  OnClickListener

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_scan:
                //  check state
                switch (this.state) {
                    case STATE_READY:
                        //  start scanning
                        this.startScan();
                        break;
                    case STATE_RECORDING:
                        //  abort recording
                        this.abortScan();
                        break;
                    case STATE_COMPLETED:
                        //  return to barcode activity
                        this.finish();
                        //  override the transition
                        this.overridePendingTransition(R.anim.animation_fadein, R.anim.animation_exit);
                        break;
                }
                break;
            case R.id.button_cancel:
                //  forget patient badge
                Settings.shared.setPatient(null);
                //  check state
                if (this.state != STATE_COMPLETED) {
                    //  return to barcode activity
                    this.finish();
                } else {
                    //  return to main activity after clearing stack
                    final Intent intent = new Intent(this, BadgeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                //  override the transition
                this.overridePendingTransition(R.anim.animation_fadein, R.anim.animation_exit);
                break;
        }
    }
}
