//  Settings.java

//  persistent storage for application settings

package net.pupilscan.app;

import android.content.Context;
import android.content.SharedPreferences;

public class Settings {

    /*
        from PupilScan iOS app:
        static let authorizedKey = "authorized"
        static let databaseKey = "database"
        static let locationKey = "location"
        static let overlayKey = "overlay"
        static let pilotKey = "pilot"
        static let speechKey = "speech"
    */

    //  constants

    private static final String PREFERENCES_NAME = "PupilScanPreferences";
    private static final String KEY_ATTEST = "Development";
    private static final String KEY_AUDIO= "Audio";
    private static final String KEY_DEVELOPMENT = "Development";
    private static final String KEY_FRONT_CAMERA = "FrontCamera";
    private static final String KEY_BADGE = "Badge";
    private static final String KEY_PATIENT = "Patient";

    //  singleton

    public static Settings shared = new Settings();

    //  instance variables

    //private Boolean mDevelopment = false;

    //  constructor

    private Settings() {
        //  empty for now
    }

    //  public getters and setters

    public Integer getAttest() {
        return this.getInt(KEY_ATTEST, 0);
    }
    public void setAttest(Integer attest) {
        this.putInt(KEY_ATTEST, attest);
    }
    public Boolean getAudio() {
        return this.getBoolean(KEY_AUDIO, false);
    }
    public void setAudio(Boolean audio) {
        this.putBoolean(KEY_AUDIO, audio);
    }
    public Boolean getDevelopment() {
        return this.getBoolean(KEY_DEVELOPMENT, false);
    }
    public void setDevelopment(Boolean development) {
        this.putBoolean(KEY_DEVELOPMENT, development);
    }
    public Boolean getFrontCamera() {
        return this.getBoolean(KEY_FRONT_CAMERA, false);
    }
    public void setFrontCamera(Boolean frontCamera) {
        this.putBoolean(KEY_FRONT_CAMERA, frontCamera);
    }
    public String getBadge() { return this.getString(KEY_BADGE, null); }
    public void setBadge(String badge) { this.putString(KEY_BADGE, badge); }
    public String getPatient() { return this.getString(KEY_PATIENT, null); }
    public void setPatient(String patient) { this.putString(KEY_PATIENT, patient); }

    //  methods

    private Integer getInt(String key, Integer dflt) {
        //  get editor
        SharedPreferences prefs = this.getPrefs();
        //  return retrieved value or default
        return prefs.getInt(key, dflt);
    }

    private void putInt(String key, Integer value) {
        //  get editor
        SharedPreferences.Editor editor = this.getEditor();
        //  put value and commit
        editor.putInt(key, value.intValue()).commit();
    }

    private Boolean getBoolean(String key, Boolean dflt) {
        //  get editor
        SharedPreferences prefs = this.getPrefs();
        //  return retrieved value or default
        return prefs.getBoolean(key, dflt);
    }

    private void putBoolean(String key, Boolean value) {
        //  get editor
        SharedPreferences.Editor editor = this.getEditor();
        //  put value and commit
        editor.putBoolean(key, value).commit();
    }

    private String getString(String key, String dflt) {
        //  get editor
        SharedPreferences prefs = this.getPrefs();
        //  return retrieved value or default
        return prefs.getString(key, dflt);
    }

    private void putString(String key, String value) {
        //  get editor
        SharedPreferences.Editor editor = this.getEditor();
        //  put value and commit
        editor.putString(key, value).commit();
    }

    private SharedPreferences getPrefs() {
        //  get application context
        Context context = GlobalApplication.getContext();
        //  get shared preferences
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor() {
        //  return preferences editor
        return this.getPrefs().edit();
    }
}
