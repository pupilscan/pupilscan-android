//  BarcodeActivity.java

//  type in or scan patient barcode

package net.pupilscan.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Bundle;
import android.os.HandlerThread;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Size;
import android.util.SizeF;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class BarcodeActivity extends CameraActivity implements View.OnClickListener, TextureView.SurfaceTextureListener {

    //  constants

    private static final String TAG = "BadgeActivity";
    private static final String MESSAGE_BARCODE = "barcode";
    private static final String MESSAGE_EXTRA = "extra";

    //  instance views and widgets

    private Button buttonBadge;
    private Button buttonCancel;
    private ImageButton buttonCamera;
    private ImageButton buttonTorch;

    //  instance variables

    private FirebaseVisionBarcodeDetector barcodeDetector;

    //  static variables

    private static int cameraFacing = CameraCharacteristics.LENS_FACING_BACK;

    //  listener & receiver classes (not interfaces)

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ((intent.getAction() == MESSAGE_BARCODE) && intent.hasExtra(MESSAGE_EXTRA)) {
                String barcode = intent.getStringExtra(MESSAGE_EXTRA);
                if (barcode != null) {
                    //  store patient barcode
                    Settings.shared.setPatient(barcode);
                    //  continue to face activity
                    Log.d(TAG, "onReceive  continue to face activity");
                    //  TODO:
                }
            }
        }
    };

    //  overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  call superclass method
        super.onCreate(savedInstanceState);
        //  set content view using the container layout
        this.setContentView(R.layout.barcode_activity);
        //  barcode detector
        this.barcodeDetector = FirebaseVision.getInstance().getVisionBarcodeDetector();
        //  assign instance
        this.textureView = this.findViewById(R.id.texture_barcode);
        this.buttonBadge = this.findViewById(R.id.button_barcode);
        this.buttonCancel = this.findViewById(R.id.button_cancel);
        this.buttonCamera = this.findViewById(R.id.button_camera);
        this.buttonTorch = this.findViewById(R.id.button_torch);
        //  attach listeners
        this.buttonBadge.setOnClickListener(this);
        this.buttonCancel.setOnClickListener(this);
        this.buttonCamera.setOnClickListener(this);
        this.buttonTorch.setOnClickListener(this);
        //  set button states
        this.updateUI();
    }

    @Override
    public void onResume() {
        //  call superclass method
        super.onResume();
        //  set as receiver
        IntentFilter barcodeIntentFilter = new IntentFilter();
        barcodeIntentFilter.addAction(MESSAGE_BARCODE);
        this.registerReceiver(this.messageReceiver, barcodeIntentFilter);
    }

    @Override
    public void onPause() {
        //  stop messaging
        this.unregisterReceiver(this.messageReceiver);
        //  call superclass method
        super.onPause();
    }

    //  instance methods

    private void updateUI() {
        //  camera
        this.buttonCamera.setEnabled(((this.cameraList != null) && (this.cameraList.length > 0)));
        //  torch
        this.buttonTorch.setEnabled(this.hasTorch);
    }

    @Override
    protected void processBitmap(@NonNull Bitmap bitmap) {
        //  check arguments
        if ((bitmap != null) && (this.barcodeDetector != null)) {
            //  convert from bitmap to FirebaseVisionImage
            FirebaseVisionImage visionImage = FirebaseVisionImage.fromBitmap(bitmap);
            if (visionImage != null) {
                //  start barcode detection (performed in background)
                barcodeDetector.detectInImage(visionImage)
                        .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                            @Override
                            public void onSuccess(List<FirebaseVisionBarcode> firebaseVisionBarcodes) {
                                //Log.d(TAG, "processBitmap  OnSuccess  firebaseVisionBarcodes=" + firebaseVisionBarcodes);
                                if (!firebaseVisionBarcodes.isEmpty())
                                    for (FirebaseVisionBarcode barcode : firebaseVisionBarcodes) {
                                        //  populate edit text with barcode
                                        Intent barcodeIntent = new Intent();
                                        barcodeIntent.setAction(MESSAGE_BARCODE);
                                        barcodeIntent.putExtra(MESSAGE_EXTRA, barcode.getRawValue());
                                        BarcodeActivity.this.sendBroadcast(barcodeIntent);
                                        Log.d(TAG, "processBitmap  barcode=" + barcode.getRawValue());
                                    }
                                //  release semaphore
                                BarcodeActivity.super.processBitmap(bitmap);
                            }
                        })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.e(TAG, "processBitmap  OnFailureListener  e=" + e.getLocalizedMessage());
                                        //  release semaphore
                                        BarcodeActivity.super.processBitmap(bitmap);
                                    }
                                });
            } else
                //  release semaphore
                super.processBitmap(bitmap);
        } else
            //  release semaphore
            super.processBitmap(bitmap);
    }

    //  OnClickListener

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_barcode:
                //  switch to face activity
                Intent intent = new Intent(this, FaceActivity.class);
                this.startActivity(intent);
                //  animate
                this.overridePendingTransition(R.anim.animation_enter, R.anim.animation_fadeout);
                break;
            case R.id.button_cancel:
                //  forget clinician badge
                Settings.shared.setBadge(null);
                //  return to badge activity
                this.finish();
                //  override the transition
                this.overridePendingTransition(R.anim.animation_fadein, R.anim.animation_exit);
                break;
            case R.id.button_camera:
                if ((this.cameraList != null) && (this.cameraList.length > 1)) {
                    //  close current camera
                    this.closeCamera();
                    //  next camera
                    this.cameraSelected = (this.cameraSelected + 1) % this.cameraList.length;
                    //  open camera
                    this.openCamera();
                }
                break;
            case R.id.button_torch:
                if (this.hasTorch) {
                    //  toggle torch
                    this.isTorchOn = !this.isTorchOn;
                    //  update camera
                    this.updateSession();
                    //  update user interface (is this needed?)
                    //this.updateUI();
                }
                break;
        }
    }
}
