//  BadgeActivity.java

//  type in or scan clinician badge

package net.pupilscan.app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Bundle;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.util.SizeF;
import android.view.Surface;
import android.view.TextureView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class CameraActivity extends BaseActivity implements TextureView.SurfaceTextureListener {

    //  constants

    private static final String TAG = "CameraActivity";
    private static final long SEMAPHORE_TIMEOUT = 2500;
    private static final long PROCESS_TIMEOUT = 30;
    private static final long PAUSE_TIMEOUT = 1000;

    //  instance views

    protected TextureView textureView;

    //  protected instance variables

    protected String[] cameraList;
    protected Integer cameraSelected;
    protected boolean hasTorch = true;
    protected boolean isTorchOn = false;
    protected int cameraOrientation;
    protected Size cameraSize;

    //  instance variables

    private BackgroundThread backgroundThread;
    private BackgroundThread processThread;
    private Semaphore cameraSemaphore = new Semaphore(1);
    private Semaphore processSemaphore = new Semaphore(1);
    private CameraDevice cameraDevice;
    private CameraCaptureSession captureSession;
    private Size previewSize;
    private CaptureRequest.Builder previewBuilder;

    //  static variables

    private static int cameraFacing = CameraCharacteristics.LENS_FACING_FRONT;

    //  listener & receiver classes (not interfaces)

    private CameraCaptureSession.CaptureCallback cameraCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            //  call superclass method
            super.onCaptureCompleted(session, request, result);
            //  process image
//            if (processSemaphore.availablePermits() > 0) {
//                //  retrieve image bitmap in current thread
//                Bitmap bitmap = textureView.getBitmap();
//                if (bitmap != null)
//                    //  process in background
//                    CameraActivity.this.processThread.handler().post(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                if (processSemaphore.tryAcquire(PROCESS_TIMEOUT, TimeUnit.MILLISECONDS))
//                                    //  process bitmap
//                                    processBitmap(bitmap);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                                //  release semaphore
//                                processSemaphore.release();
//                            }
//                        }
//                    });
//                else
//                    Log.d(TAG, "onCaptureCompleted  unable to get bitmap");
//            }
            try {
                if (processSemaphore.tryAcquire(PROCESS_TIMEOUT, TimeUnit.MILLISECONDS)) {
                    //  retrieve image bitmap in current thread
                    Bitmap bitmap = textureView.getBitmap();
                    if (bitmap != null) {
                        //  process bitmap
                        processBitmap(bitmap);
                    } else {
                        //  release semaphore
                        processSemaphore.release();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                //  release semaphore
                processSemaphore.release();
            }
        }
    };
    private CameraDevice.StateCallback cameraStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice aCameraDevice) {
            //  remember camera device
            cameraDevice = aCameraDevice;
            //  begin preview
            startSession();
            //  release semaphore
            cameraSemaphore.release();
            //  configure transform
            configureTransform();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            //  release semaphore
            cameraSemaphore.release();
            //  close camera device
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            //  release semaphore
            cameraSemaphore.release();
            //  close camera device
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }
            //  display error
            ErrorDialog.show("Camera failed with error code " + error, CameraActivity.this);
        }
    };

    //  overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  call superclass method
        super.onCreate(savedInstanceState);
        //  check permissions
        PermissionsUtility.checkPermissions(this);
        //  torch availability
        this.hasTorch = this.getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    @Override
    public void onResume() {
        //  call superclass method
        super.onResume();
        //  start the background threads
        this.backgroundThread = new BackgroundThread("BackgroundThread");
        this.processThread = new BackgroundThread("ProcessThread");
        //this.processThread = new BackgroundThread("ProcessThread", HandlerThread.MIN_PRIORITY);
        //  connect camera with texture view
        if (this.textureView.isAvailable())
            this.openCamera();
        else
            this.textureView.setSurfaceTextureListener(this);
    }

    @Override
    public void onPause() {
        //  close the camera
        this.closeCamera();
        //  wait for processing to complete
        try {
            processSemaphore.tryAcquire(PAUSE_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Log.e(TAG, "onPause  unable to acquire process semaphore");
        } finally {
            processSemaphore.release();
        }
        //  clear texture listener
        if (this.textureView != null)
            this.textureView.setSurfaceTextureListener(null);
        //  stop background threads
        if (this.backgroundThread != null) {
            this.backgroundThread.stop();
            this.backgroundThread = null;
        }
        if (this.processThread != null) {
            this.processThread.stop();
            this.processThread = null;
        }
        //  call superclass method
        super.onPause();
    }

    //  instance methods

    @SuppressWarnings("MissingPermission")
    protected void openCamera() {
        //  make sure activity is not finishing
        if (this.isFinishing())
            return;
        //  get camera manager
        CameraManager cameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
        try {
            //  lock camera
            if (!this.cameraSemaphore.tryAcquire(SEMAPHORE_TIMEOUT, TimeUnit.MILLISECONDS)) {
                Log.e(TAG, "openCamera  unable to lock camera");
                ErrorDialog.show("Unable to lock camera", this);
            }
            //  retrieve camera list
            if (this.cameraList == null)
                this.cameraList = cameraManager.getCameraIdList();
            if ((this.cameraList == null) || (this.cameraList.length == 0))
                return;
            //  default camera
            if (this.cameraSelected == null) {
                this.cameraSelected = 0;
                for (int i = 0; i < this.cameraList.length; i++) {
                    CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(this.cameraList[i]);
                    if ((cameraCharacteristics != null) && (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CameraActivity.cameraFacing)) {
                        this.cameraSelected = i;
                        break;
                    }
                }
            } else if (this.cameraSelected >= this.cameraList.length)
                this.cameraSelected = 0;
            //  get camera specs
            CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(this.cameraList[this.cameraSelected]);
            if (cameraCharacteristics != null) {
                //  choose camera resolution
                StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (streamConfigurationMap != null) {
                    //  retrieve output sizes
                    Size[] cameraSizes = streamConfigurationMap.getOutputSizes(SurfaceTexture.class);
                    if ((cameraSizes != null) && (cameraSizes.length > 0)) {
                        //  find highest-resolution size for camera
                        this.cameraSize = cameraSizes[0];      //    default
                        int pixelCount = 0;
                        for (Size size : cameraSizes)
                            if (pixelCount < size.getWidth() * size.getHeight()) {
                                //  remember pixel count
                                pixelCount = size.getWidth() * size.getHeight();
                                //  set camera size (may be overridden)
                                this.cameraSize = size;
                            }
                        //  choose optimal preview size for this camera with similar aspect ratio
                        this.previewSize = new Size(this.cameraSize.getWidth(), this.cameraSize.getHeight());
                        pixelCount = 0;
                        for (Size size : cameraSizes)
                            //  need same aspect ratio, and size at least as big as camera
                            if ((this.cameraSize.getWidth() * size.getHeight() == size.getWidth() * this.cameraSize.getHeight()) &&
                                    (size.getWidth() >= this.textureView.getWidth()) &&
                                    (size.getHeight() >= this.textureView.getHeight())) {
                                //  check pixel count
                                if ((pixelCount == 0) || (pixelCount > size.getWidth() * size.getHeight())) {
                                    //  remember pixel count
                                    pixelCount = size.getWidth() * size.getHeight();
                                    //  prefer this preview size - smaller than the max and same aspect ratio
                                    this.previewSize = size;
                                }
                            }
                        //  configure transform from camera to preview
                        this.configureTransform();
                        //  open camera
                        cameraManager.openCamera(this.cameraList[this.cameraSelected], this.cameraStateCallback, null);
                        //  remember orientation
                        this.cameraOrientation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                    }
                }
            } else
                Log.e(TAG, "openCamera  unable to get camera characteristics for camera " + this.cameraSelected);
        } catch (CameraAccessException e) {
            e.printStackTrace();
            ErrorDialog.show("Unable to access camera", this);
        } catch (NullPointerException e) {
            //  thrown if Camera2API not supported
            ErrorDialog.show(getString(R.string.camera_error), this);
        } catch (InterruptedException e) {
            ErrorDialog.show("Unable to exclusively access camera", this);
        }
    }

    protected void closeCamera() {
        try {
            //  lock camera
            this.cameraSemaphore.acquire();
            this.closeSession();
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }
        } catch (InterruptedException e) {
            ErrorDialog.show("Unable to close camera", this);
        } finally {
            //  release semaphore
            this.cameraSemaphore.release();
        }
    }

    private void configureTransform() {
        //  check arguments
        if ((this.textureView != null) && (this.cameraSize != null) && (this.previewSize != null)) {
            //  get texture size & center
            SizeF textureSize = new SizeF(this.textureView.getWidth(), this.textureView.getHeight());
            //  set matrix - first map then overscan
            Matrix matrix = new Matrix();
            float scaleX = this.previewSize.getWidth() / textureSize.getWidth();
            float scaleY = this.previewSize.getHeight() / textureSize.getHeight();
            if (textureSize.getWidth() * this.previewSize.getHeight() < this.previewSize.getWidth() * textureSize.getHeight())
                scaleY *= textureSize.getHeight() / textureSize.getWidth();
            else
                scaleX *= textureSize.getWidth() / textureSize.getHeight();
            matrix.postScale(scaleX, scaleY, textureSize.getWidth() / 2, textureSize.getHeight() / 2);
            //  assign matrix to texture view
            textureView.setTransform(matrix);
        }
    }

    protected void startSession() {
        if ((this.cameraDevice != null) && this.textureView.isAvailable() && (this.previewSize != null)) {
            try {
                //  close existing preview session
                this.closeSession();
                //  get surface texture
                SurfaceTexture surfaceTexture = this.textureView.getSurfaceTexture();
                if (surfaceTexture != null) {
                    //  set surface texture to same size as preview size
                    surfaceTexture.setDefaultBufferSize(this.previewSize.getWidth(), this.previewSize.getHeight());
                    //  create preview builder
                    this.previewBuilder = this.cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                    if (this.previewBuilder != null) {
                        //  create new surface
                        Surface previewSurface = new Surface(surfaceTexture);
                        if (previewSurface != null) {
                            //  create surface targets
                            ArrayList<Surface> surfaces = new ArrayList<Surface>();
                            surfaces.add(previewSurface);
                            this.previewBuilder.addTarget(previewSurface);
                            //  add supplmental surface
                            Surface surface = this.supplementalSurface();
                            if (surface != null) {
                                surfaces.add(surface);
                                this.previewBuilder.addTarget(surface);
                            }
                            //  create capture session
                            this.cameraDevice.createCaptureSession(surfaces,
                                    new CameraCaptureSession.StateCallback() {

                                        @Override
                                        public void onConfigured(@NonNull CameraCaptureSession session) {
                                            //  remember preview session
                                            captureSession = session;
                                            //  update the preview session
                                            updateSession();
                                            //  session callback
                                            sessionCallback();
                                        }

                                        @Override
                                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                                            Log.e(TAG, "startSession  unable to configure camera");
                                            ErrorDialog.show("Unable to configure camera", CameraActivity.this);
                                        }
                                    }, this.backgroundThread.handler());
                        } else
                            Log.e(TAG, "startSession  unable to create new surface");
                    } else
                        Log.e(TAG, "startSession  unable to get preview capture request");
                } else
                    Log.e(TAG, "startSession  unable to get get surface texture");
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    protected Surface supplementalSurface() {
        return null;
    }

    protected void sessionCallback() {
    }

    protected void updateSession() {
        if ((this.cameraDevice != null) && (this.previewBuilder != null) && (this.captureSession != null)) {
            try {
                //  automatic control
                this.previewBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
                //  torch
                this.previewBuilder.set(CaptureRequest.FLASH_MODE, this.isTorchOn ? CaptureRequest.FLASH_MODE_TORCH : CaptureRequest.FLASH_MODE_OFF);
                //  make request
                this.captureSession.setRepeatingRequest(previewBuilder.build(), this.cameraCaptureCallback, this.backgroundThread.handler());
            } catch (CameraAccessException e) {
                e.printStackTrace();
                Log.e(TAG, "updateSession  unable to update preview");
            }
        }
    }

    protected void closeSession() {
        if (this.captureSession != null) {
            this.captureSession.close();
            this.captureSession = null;
        }
    }

    protected void processBitmap(@NonNull Bitmap bitmap) {
        //  release semaphore
        processSemaphore.release();
    }

    //  TextureView.SurfaceTextureListener

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        //  open camera
        this.openCamera();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
        //  reconfigure transformation matrix
        this.configureTransform();
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }
}
