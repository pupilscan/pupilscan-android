//  TabBarFragment.java

//  framgent for top navigation of tabs

package net.pupilscan.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class TabBarFragment extends Fragment implements View.OnClickListener {

    //  constants

    private static final String TAG = "TabBarFragment";

    //  widgets

    private ImageButton buttonBack;
    private Button buttonTitle;

    //  overrides

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //  call superclass method
        super.onCreateView(inflater, container, savedInstanceState);
        //  convert from XML layout to fragment
        View rootView = inflater.inflate(R.layout.tabbar_fragment, container, false);
        //  declare views
        this.buttonBack = rootView.findViewById(R.id.button_back);
        this.buttonTitle = rootView.findViewById(R.id.button_tabbar);
        //  add listeners
        this.buttonBack.setOnClickListener(this);
        this.buttonTitle.setOnClickListener(this);
        return rootView;
    }

    //  OnClickListener

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_back:
            case R.id.button_tabbar:
                //  return to previous activity
                this.getActivity().finish();
                //  override the transition
                this.getActivity().overridePendingTransition(R.anim.animation_fadein, R.anim.animation_exit);
                break;
        }
    }
}