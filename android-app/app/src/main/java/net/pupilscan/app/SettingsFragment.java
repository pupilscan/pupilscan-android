//  SettingsFragment.java

//  settings fragment of tabbed views

package net.pupilscan.app;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.AppCompatButton;

public class SettingsFragment extends Fragment implements View.OnClickListener, SwitchCompat.OnCheckedChangeListener {

    //  constants

    private static final String TAG = "SettingsFragment";

    //  instance variables

    private SwitchCompat audioSwitch;
    private TextView frontCameraText;
    private SwitchCompat frontCameraSwitch;
    private TextView developmentText;
    private SwitchCompat developmentSwitch;
    private Button advancedButton;

    //  static variables

    private static Boolean showAdvanced = false;

    //  overrides

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //  call superclass method
        super.onCreateView(inflater, container, savedInstanceState);
        //  convert from XML layout to fragment
        View rootView = inflater.inflate(R.layout.settings_fragment, container, false);
        //  find widgets
        audioSwitch = rootView.findViewById(R.id.switch_audio);
        frontCameraText = rootView.findViewById(R.id.text_front_camera);
        frontCameraSwitch = rootView.findViewById(R.id.switch_front_camera);
        developmentText = rootView.findViewById(R.id.text_development);
        developmentSwitch = rootView.findViewById(R.id.switch_development);
        advancedButton = rootView.findViewById(R.id.button_advanced);
        //  add listeners
        audioSwitch.setOnCheckedChangeListener(this);
        frontCameraSwitch.setOnCheckedChangeListener(this);
        developmentSwitch.setOnCheckedChangeListener(this);
        advancedButton.setOnClickListener(this);
        //  set states
        audioSwitch.setChecked(Settings.shared.getAudio());
        frontCameraSwitch.setChecked(Settings.shared.getFrontCamera());
        developmentSwitch.setChecked(Settings.shared.getDevelopment());
        //  setup widgets
        this.setup();
        //  done
        return rootView;
    }

    //  methods

    private void setup() {
        int visibility = showAdvanced ? View.VISIBLE : View.INVISIBLE;
        //  front camera
        frontCameraText.setVisibility(visibility);
        frontCameraSwitch.setVisibility(visibility);
        //  development
        developmentText.setVisibility(visibility);
        developmentSwitch.setVisibility(visibility);
        //  advanced button
        advancedButton.setText(this.getString(showAdvanced ? R.string.advanced_hide : R.string.advanced_show));
    }

    //  OnClickListener

    @Override
    public void onClick (View view) {
        Log.d(TAG, "onClick");
        if (view.getId() == R.id.button_advanced) {
            //  toggle advanced widgets
            showAdvanced = !showAdvanced;
            //  show/hide widgets
            this.setup();
        }
    }

    //  OnCheckedChangeListener

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d(TAG, "onCheckedChanged  buttonView=" + buttonView);
        switch (buttonView.getId()) {
            case R.id.switch_audio:
                Settings.shared.setAudio(isChecked);
                break;
            case R.id.switch_front_camera:
                Settings.shared.setFrontCamera(isChecked);
                break;
            case R.id.switch_development:
                Settings.shared.setDevelopment(isChecked);
                break;
        }
    }
}