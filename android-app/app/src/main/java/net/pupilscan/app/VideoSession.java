package net.pupilscan.app;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;

import java.util.ArrayList;


public class VideoSession {

    //  CONSTANTS

    private static final String TAG = "VideoSession";


    //  INSTANCE VARIABLES

    private CaptureRequest.Builder cameraCaptureRequest;
    private ArrayList<Surface> surfaceList = new ArrayList<Surface>();
    private CameraManager cameraManager;
    private CameraDevice cameraDevice;
    private CameraCaptureSession videoCaptureSession;
    private String cameraName;


    //  CALLBACKS

    private CameraDevice.StateCallback videoDeviceStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice camera) {
            // grab the camera (we need to grab it so we can toggle flash state later)
            cameraDevice = camera;
            // start with the torch off by defaultd
            startCapture(false);
        }

        @Override
        public void onClosed(CameraDevice camera) {
            Log.i(TAG, "StateCallback  camera closed");
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            Log.i(TAG, "StateCallback  camera disconnected");
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            Log.i(TAG, "StateCallback  camera error");
        }

    };

    private CameraCaptureSession.StateCallback videoDeviceSessionStateCallback = new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
            //  check dependent arguments
            if (cameraCaptureRequest != null) {
                // grab the CameraCaptureSession by reference
                videoCaptureSession = cameraCaptureSession;  // this is a variable for the entire VideoSession object
                // build the request
                CaptureRequest builtCaptureRequest = cameraCaptureRequest.build();
                try {
                    // set to make continual requests
                    videoCaptureSession.setRepeatingRequest(builtCaptureRequest, null, null);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                    Log.e(TAG, "CameraCaptureSession.StateCallback  error=" + e.getLocalizedMessage());
                }
            } else
                Log.e(TAG, "createNewCaptureSessionStateCallback  WARNING  cameraCaptureRequest is null");
        }

        @Override
        public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
            Log.i(TAG, "createNewCaptureSessionStateCallback  configuration failed");
        }
    };


    //  CONSTRUCTOR

    public VideoSession(CameraManager cameraManager, String cameraName) {
        this.cameraManager = cameraManager;  // get the cameraManager we are using in the whole activity
        this.cameraName = cameraName;  // the name of the camera, as a string. We get this earlier from the CameraManager, via getCameraIdList
    }

    //  METHODS

    private void createNewCaptureSessionStateCallback() {
        //////////////////////////////////////////////////
        // This method will create for us a new set of callbacks, and what to do
        //

        // create the callbacks for the session
        videoDeviceSessionStateCallback = new CameraCaptureSession.StateCallback() {
            @Override
            public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                //  check dependent arguments
                if (cameraCaptureRequest != null) {
                    // grab the CameraCaptureSession by reference
                    videoCaptureSession = cameraCaptureSession;  // this is a variable for the entire VideoSession object
                    // build the request
                    CaptureRequest builtCaptureRequest = cameraCaptureRequest.build();
                    try {
                        // set to make continual requests
                        videoCaptureSession.setRepeatingRequest(builtCaptureRequest, null, null);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                } else
                    Log.e(TAG, "createNewCaptureSessionStateCallback  WARNING  cameraCaptureRequest or videoCaptureSession is null");
            }

            @Override
            public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                Log.i(TAG, "createNewCaptureSessionStateCallback  configuration failed");
            }
        };

    }

    public void toggleFlashState(boolean desiredFlashState) {
        ////////////////////////////
        // Toggle flash state on and off
        //  (true for on, false for off)
        //

        // Here we will make a new capture request with the desired configuration:

        // turn on or off flash, as requested
        cameraCaptureRequest.set(CaptureRequest.FLASH_MODE, desiredFlashState ? CaptureRequest.FLASH_MODE_TORCH : CaptureRequest.FLASH_MODE_OFF);
        // build the request
        CaptureRequest builtCaptureRequest = cameraCaptureRequest.build();
        try {
            // stop the repeating request that currently exists
            videoCaptureSession.stopRepeating();
            // create a new repeating request with the new configuration
            videoCaptureSession.setRepeatingRequest(builtCaptureRequest, null, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
            Log.e(TAG, "toggleFlashState  error=" + e.getLocalizedMessage());
            //  FUTURE: CONSIDER DATABASE LOG RECORD
            //  Database.shared.addLog(...)
        }
    }

    public void turnOnVideoCamera(ArrayList<Surface> surfaceList) {
        ///////////////////////////
        //
        //

        // get the surfaces to be associated with this VideoSession instance. These will be needed
        // once the camera is opened, b/c of videoDeviceStateCallback.onOpened()
        this.surfaceList = surfaceList;

        // open up the camera
        try {
            cameraManager.openCamera(cameraName, videoDeviceStateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
            Log.e(TAG, "turnOnVideoCamera  access error=" + e.getLocalizedMessage());
        } catch (SecurityException e) {
            e.printStackTrace();
            Log.e(TAG, "turnOnVideoCamera  security error=" + e.getLocalizedMessage());
        }
    }

    public void turnOffVideoCamera() {
        //  check camera device
        if (cameraDevice != null) {
            //  close camera device
            cameraDevice.close();
            Log.i(TAG, "turnOffVideoCamera  camera is now off");
        } else
            Log.e(TAG, "turnOffVideoCamera  trying to turn off NULL camera device");
    }


    // OBJECTS

    public void startCapture(boolean desiredTorchState) {
        //////////////////////////
        //
        //

        //  make sure cameraDevice is not null
        if (cameraDevice != null) {
            try {
                // create a capture request
                cameraCaptureRequest = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            } catch (CameraAccessException e) {
                e.printStackTrace();
                Log.e(TAG, "startCapture  acess error=" + e.getLocalizedMessage());
            }


            // add all the needed surfaces
            surfaceList.forEach(
                    //  lambda expression for adding surface to camera targets
                    (surface) -> cameraCaptureRequest.addTarget(surface)
            );

            // turn off autofocus (may not be needed - needs testing)
            cameraCaptureRequest.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);

            //Handler newCaptureSessionHandler = new Handler();  // a named handler for the session (idk why we need this, may not)

            // create the actual capture session
            //  CHECK - videoDeviceSessionStateCallback is only instantiated once
            try {
                cameraDevice.createCaptureSession(surfaceList, videoDeviceSessionStateCallback, null);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        } else
            Log.e(TAG, "startCapture  trying to start NULL camera device");
    }
}