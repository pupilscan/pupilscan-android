//  PermissionsUtility.java

//  manages permission requirements

package net.pupilscan.app;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.io.File;

public class FileUtility {

    //  constants

    private static final String VIDEOS = "videos";
    private static final String UPLOADS = "uploads";
    private static final String SCANS = "scans";
    private static final String CACHE = "cache";
    private static final String TAG = "FileUtility";

    //  static methods

    private static File getDirectoryFile(String directoryType) {
        //  start with global activity
        Context context = GlobalApplication.getContext();
        //  get document directory
        File directory = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        //  check subfolder
        File directorySub = new File(directory.getAbsolutePath() + "/" + directoryType);
        if (!directorySub.exists())
            directorySub.mkdir();
        return directorySub;
    }

    private static String getDirectory(String directoryType) {
        return getDirectoryFile(directoryType).getAbsolutePath();
    }

    public static String videoPath(String filename) {
        return getDirectory(VIDEOS) + "/" + filename;
    }
    public static File videoFile(String filename) {
        return new File(getDirectory(VIDEOS), filename);
    }
}
