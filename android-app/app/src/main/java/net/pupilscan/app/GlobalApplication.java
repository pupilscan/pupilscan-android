//  GlobalApplication.java

//  extends the Application class for global context

package net.pupilscan.app;

import android.app.Application;
import android.content.Context;

import com.google.firebase.FirebaseApp;

public class GlobalApplication extends Application {

    //  static variables

    private static Context appContext;

    //  overrides

    @Override
    public void onCreate() {
        //  call superclass method
        super.onCreate();
        //  save application context
        appContext = this.getApplicationContext();
        //  gather network addresses
        NetworkService.getIpAddress();
        NetworkService.getPublicIP();
    }

    //  methods

    public static Context getContext() {
        return appContext;
    }
}
