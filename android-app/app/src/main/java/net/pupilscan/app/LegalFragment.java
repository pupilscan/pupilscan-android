//  LegalFragment.java

//  framgent for legal terms

package net.pupilscan.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class LegalFragment extends Fragment {

    //  instance variables

    //private LegalViewModel legalViewModel;

    //  overrides

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //  call superclass method
        super.onCreateView(inflater, container, savedInstanceState);
        //  create view model
        //legalViewModel = ViewModelProviders.of(this).get(LegalViewModel.class);
        //  convert from XML layout to fragment
        View root = inflater.inflate(R.layout.legal_fragment, container, false);
        //final TextView textView = root.findViewById(R.id.text_legal);
        //legalViewModel.getText().observe(this, new Observer<String>() {
        //    @Override
        //    public void onChanged(@Nullable String s) {
        //        textView.setText(s);
        //    }
        //});
        return root;
    }
}