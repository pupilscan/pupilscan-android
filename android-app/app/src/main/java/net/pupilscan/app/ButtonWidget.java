package net.pupilscan.app;

import android.content.Context;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatButton;

public class ButtonWidget extends AppCompatButton {

    //  static variables

    private static final int[] STATE_DANGER = { R.attr.state_danger };

    //  instance variables

    private boolean isDanger = false;

    //  constructor

    public ButtonWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //  overrides

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        //  check for danger
        if (this.isDanger) {
            //  call superclass method
            final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
            //  merge custom state
            return mergeDrawableStates(drawableState, STATE_DANGER);
        } else
            //  return superclass result
            return super.onCreateDrawableState(extraSpace);
    }

    //  methods

    public void setDanger(boolean isDanger) {
        //  set danger state
        this.isDanger = isDanger;
    }
}
