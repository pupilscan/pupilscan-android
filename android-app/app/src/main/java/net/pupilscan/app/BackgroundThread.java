//  BackgroundThread.java

//  encapsulates a thread and handler for background processing

package net.pupilscan.app;

import android.os.Handler;
import android.os.HandlerThread;

class BackgroundThread {

    //  constants

    //private static final String THREAD_NAME = "BackgroundThread";

    //  instance variables

    Handler mHandler;
    HandlerThread mHandlerThread;

    //  constructor

    public BackgroundThread(String threadName, int priority) {
        //  start thread
        mHandlerThread = new HandlerThread(threadName);
        mHandlerThread.setPriority(priority);
        mHandlerThread.start();
        //  create handler
        mHandler = new Handler(mHandlerThread.getLooper());
    }

    public BackgroundThread(String threadName) {
        this(threadName, HandlerThread.NORM_PRIORITY);
    }

    //  methods

    public Handler handler() {
        return mHandler;
    }

    public void stop() {
        //  check to make sure it is running
        if (mHandlerThread != null) {
            //  quit background thread
            mHandlerThread.quitSafely();
            try {
                //  wait for thread to die
                mHandlerThread.join();
                //  clear instance variables
                mHandlerThread = null;
                mHandler = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
