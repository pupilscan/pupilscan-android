//  TabContainerActivity.java

//  container for tabbed views

package net.pupilscan.app;

import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class TabContainerActivity extends BaseActivity {

    //  overrides

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //  call superclass method
        super.onCreate(savedInstanceState);
        //  set the content view
        this.setContentView(R.layout.tab_activity);
        //  fimd the bottom navigation view
        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        //  declare fragment identifiers
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.tab_scans, R.id.tab_settings, R.id.tab_system,
                R.id.tab_legal, R.id.tab_about)
                .build();
        //  find the navigation controller
        NavController navController = Navigation.findNavController(this, R.id.navigation_fragment);
        //  connect the action bar and navigation controller
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        //  connect the navigation controller with the bottom navigation view
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }

    @Override
    public void onBackPressed() {
        //  return to previous activity
        this.finish();
        //  override the transition
        this.overridePendingTransition(R.anim.animation_fadein, R.anim.animation_exit);
    }
}
