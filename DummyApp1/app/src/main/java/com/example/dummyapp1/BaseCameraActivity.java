package com.example.dummyapp1;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import static android.graphics.ImageFormat.JPEG;
import static android.media.MediaCodec.BUFFER_FLAG_END_OF_STREAM;
import static android.media.MediaCodec.CONFIGURE_FLAG_ENCODE;
import static android.media.MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible;
import static android.media.MediaFormat.MIMETYPE_VIDEO_AVC;



public class BaseCameraActivity extends AppCompatActivity {






    Surface previewSurface;  // surface for previewTextureView; for streaming video from VideoSession
    String[] cameraList;  // list of the names of cameras, gotten from CameraManager. Needed for opening correct camera for VideoSession
    VideoSession videoSession;  // our VideoSession instance
    ArrayList<Surface> surfaceArrayList;  // we store all the surfaces we will be using with our capture session here
    TextureView.SurfaceTextureListener previewSurfaceTextureViewListener;  // this is the listener for the state of previewTextureView; we need this to know when it's ready to be used, so we can start VideoSession camera
    ImageView previewImageView;  // TextureView that displays camera feed
    ImageReader previewImageReader;  // ImageReader for the main preview

    int cameraWidth;
    int cameraHeight;
    int cameraSensorFeedOrientation;  // rotation of the camera feed relative to the physical phone. 90 for back camera and 270 for front


    ImageReader.OnImageAvailableListener previewImageListener;  // the name of the listener we will perpetually use to get images from camera for the preview


    Handler uiHandler;  // handler for updating the UI. Used with the OnImageAvailableListener for previewImageView


    Bitmap newestScanImage;


    ArrayList<Bitmap> videoArray;



    Image imageToSave;








    //////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        // ONLY NEED THIS WHEN ACCESSING APP FOR THE FIRST TIME. APP MAY ACT FUNNY UNTIL YOU ACCEPT CONDITIONS.
        // WILL PUT THIS PART INTO INITIAL LOADING SCREEN ON REAL APP
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{ android.Manifest.permission.CAMERA }, 1);
        }





        // UI SETUP  (Ultimately, this should be done in the XML)


        // hide actionbar
        getSupportActionBar().hide();

        // grab toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        // set the logo
        toolbar.setLogo(R.drawable.heading_200);
        // set the color
        toolbar.setBackgroundColor(Color.rgb(0,0,0));




        // Set & lock screen settings:
        // (prevent camera from crashing when screen is rotated)

        // put it in portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // lock it in portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        // prevent screen from dimming
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);



        // grab the main TextureView for the activity
        previewImageView = findViewById(R.id.previewImageView);







        // create our ArrayList object for holding the surfaces we will pass to VideoSession
        surfaceArrayList = new ArrayList<Surface>();






        // get CameraManager
        CameraManager cameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);



        // Get stuff about cameras available

        try {
            // list of all available cameras
            cameraList = cameraManager.getCameraIdList();
            // CameraCharacteristics for desired camera (in this case, back camera (0))
            CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraList[0]);
            // StreamConfigurationMap (which has size of camera feed (width & height)
            StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            // width & height
            cameraWidth = streamConfigurationMap.getOutputSizes(SurfaceTexture.class)[0].getWidth();
            Log.i("CAMERA WIDTH", Integer.toString(cameraWidth));
            cameraHeight = streamConfigurationMap.getOutputSizes(SurfaceTexture.class)[0].getHeight();
            Log.i("CAMERA HEIGHT", Integer.toString(cameraHeight));
            cameraSensorFeedOrientation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }





        // Grab images from camera for previewImageView

        // create image reader for camera and previewImageView
        previewImageReader = ImageReader.newInstance(cameraWidth,cameraHeight,JPEG,30);
        // create a listener for previewImageReader and previewImageView. This will grab images as they become available
        // from the camera, & send them to previewImageView
        previewImageListener = createOnImageAvailableListener(previewImageView);
        // create handler for previewImageReader
        Handler previewImageListenerHandler = new Handler();
        // set the listener to previewImageReader
        previewImageReader.setOnImageAvailableListener(previewImageListener, previewImageListenerHandler);





        // Add surfaces to our array of surfaces

        // grab surface associated w/ previewImageReader, & add to array
        Surface previewSurface = previewImageReader.getSurface();
        surfaceArrayList.add(previewSurface);




        // get the main thread as a handler. We will use this to update the UI
        uiHandler =  new Handler(Looper.getMainLooper());




        // create the VideoSession
        videoSession = new VideoSession(cameraManager, cameraList[0]);

        // turn on the camera for the session
        videoSession.turnOnVideoCamera(surfaceArrayList);



        // initialize our array for storing the images we capture from camera feed
        videoArray = new ArrayList<Bitmap>();


        //****************
        // tells us what codecs are available; this is for *debugging*
        getCodecs();
        //***************

    }
    ////////////////////////////////////////////////////////////







    // METHODS


    public void takeAScan(View view) {
        ///////////////////////////////////
        // This is how we take a scan.
        //

        // start scan; grab image at 33ms to keep at 30fps
        new CountDownTimer(500,33) {

            @Override
            public void onTick(long millisInFuture) {
                saveImage(newestScanImage);
            }

            // flash on
            @Override
            public void onFinish() {
                // turn flash on
                videoSession.toggleFlashState(true);

                new CountDownTimer(500,33) {

                    @Override
                    public void onTick(long millisInFuture) {
                        saveImage(newestScanImage);
                    }

                    // turn flash off
                    @Override
                    public void onFinish() {
                        // flash off
                        videoSession.toggleFlashState(false);


                        new CountDownTimer(5000,33) {

                            @Override
                            public void onTick(long millisInFuture) {
                                saveImage(newestScanImage);
                            }

                            @Override
                            public void onFinish() {

                                Log.i("NUMBER OF FRAMES", Integer.toString(videoArray.size()));

                                //*******************************
                                // here we use MediaCodec to create a video from the frames we grab
                                videoSession.turnOffVideoCamera(); // this is to silence input from camera so we can debug
                                                                   // easier: allows us to get all output about codec now
                                createVideo();
                                //*******************************

                            }

                        }.start();
                    }
                }.start();
            }
        }.start();


    }






    public void getCodecs() {
        //////////////////////////
        // Just prints out a list of codecs available
        //  (for debugging)
        //

        MediaCodecList mediaCodecList = new MediaCodecList(MediaCodecList.ALL_CODECS);
        MediaCodecInfo[] infoList = mediaCodecList.getCodecInfos();
        for (int i = 0; i < infoList.length; i++) {
            MediaCodecInfo info = infoList[i];
            Log.i("CODEC NAMED", info.getName());
            String[] bleh = info.getSupportedTypes();
            for (int j=0; j<bleh.length; j++) {
                Log.i("supported type", bleh[j]);
                MediaCodecInfo.CodecCapabilities capabilities = info.getCapabilitiesForType(bleh[j]);
                int[] colorList = capabilities.colorFormats;
                for (int k=0; k<colorList.length; k++) {
                    Log.i("color", Integer.toString(colorList[k]));
                }
            }
        }
    }






    public void createVideo() {
        ///////////////////////////////////
        // This creates a codec that we will use to fashion the images we collect from the camera into a video
        //


        try {
            // create the codec
            Log.i("videoCreator","Creating...");
            MediaCodec videoCreator = MediaCodec.createEncoderByType(MIMETYPE_VIDEO_AVC);
            Log.i("videoCreator", "Created.");

            // create a formatting object for codec configuration
            MediaFormat videoFormat = new MediaFormat();
            videoFormat.setString(MediaFormat.KEY_MIME, MIMETYPE_VIDEO_AVC);  // final vid format
            videoFormat.setInteger(MediaFormat.KEY_BIT_RATE, 2000000);  // in bits per sec
            videoFormat.setInteger(MediaFormat.KEY_WIDTH,600);  // the width of the images we're using
            videoFormat.setInteger(MediaFormat.KEY_HEIGHT,800);  // the height of the images we're using
            videoFormat.setInteger(MediaFormat.KEY_CAPTURE_RATE,30);  // 30fps capture
            videoFormat.setInteger(MediaFormat.KEY_FRAME_RATE, 30);  // 30fps playback
            videoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, COLOR_FormatYUV420Flexible); // only one that works with AVC
            videoFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 0);  // all frames are key frames

            // Configure the codec
            Log.i("videoCreator", "Configuring...");
            videoCreator.configure(videoFormat,null,null, CONFIGURE_FLAG_ENCODE);
            Log.i("videoCreator", "CONFIGURED");  // let us know it's configured
            // Start the codec
            videoCreator.start();
            Log.i("videoCreator","STARTED");  // let us know it's started


            int frameCounter = 0;  // will be counting how many frames we've processed in the codec

            while (frameCounter < 180) {
                // get a buffer (no buffer available if bufferIndex == -1. Then we just try again...)
                Log.i("videoCreator", "Dequeueing input buffer for frame " + frameCounter + "...");
                int bufferIndex = videoCreator.dequeueInputBuffer(-1);  // timeoutUs is in microseconds; < 0 means wait indefinitely
                Log.i("videoCreator", "Buffer dequeued");
                // check to see if we got a valid buffer, and if so,...
                if (bufferIndex != -1) {



                    Log.i("videoCreator", "Frame " + frameCounter + " being processed...");

                    // use the index of the buffer we got to get the actual input buffer
                    Log.i("videoCreator", "Getting input buffer for frame " + frameCounter + "...");
                    ByteBuffer usableBuffer = videoCreator.getInputBuffer(bufferIndex);

                    Log.i("videoCreator", "Got input buffer for frame " + frameCounter);

                    //******************************
                    Log.i("Buffer size", Integer.toString(usableBuffer.capacity()));
                    Log.i("Buffer size needed", Integer.toString(videoArray.get(frameCounter).getByteCount()));

                    Log.i("frame height", Integer.toString(videoArray.get(frameCounter).getHeight()));
                    Log.i("frame width", Integer.toString(videoArray.get(frameCounter).getWidth()));
                    //********************************




                    // & & & & & & & & & & & & & & & & & & & & & & & & & & &

                    // Convert (A)RGB to YUV here, and store result in new byte array at appropriate location


                    // get a byte array for our current image
                    byte[] bytes = new byte[videoArray.get(frameCounter).getByteCount()];

                    for (int i = 0; i < videoArray.get(frameCounter).getWidth(); i++) {
                        for (int j = 0; j < videoArray.get(frameCounter).getHeight(); j++) {


                            // get the pixel we will convert
                            int pixel = videoArray.get(frameCounter).getPixel(i,j);

                            // bitmasks to extract color values from pixel (assuming this is 32-bit ARGB)
                            int red = (pixel & 0x00ff0000) >> 16;
                            int green = (pixel & 0x0000ff00) >> 8;
                            int blue = (pixel & 0x000000ff);

                            // do conversion
                            int Y = ( (  66 * red + 129 * green +  25 * blue + 128) >> 8) +  16;
                            int U = ( ( -38 * red -  74 * green + 112 * blue + 128) >> 8) + 128;
                            int V = ( ( 112 * red -  94 * green -  18 * blue + 128) >> 8) + 128;








                        }
                    }






                    // take the current frame under consideration, and copy it to the current codec buffer
                    //videoArray.get(frameCounter).copyPixelsToBuffer(usableBuffer);
                    // queue up the input buffer we filled for the codec
                    videoCreator.queueInputBuffer(bufferIndex,0,cameraWidth*cameraHeight,0,0);  // SIZE IS NOT CORRECT

                    Log.i("videoCreator", "Frame " + frameCounter + " successfully processed.");

                    // increment our counter for the number of frames we processed
                    frameCounter += 1;

                } else {
                    Log.i("videoCreator", "Failed to dequeue buffer for frame " + frameCounter + ". Trying again...");
                }

            }



            // grab a final (valid) buffer:
            int bufferIndex = videoCreator.dequeueInputBuffer(20);
            // (keep trying until valid one is gotten)
            while (bufferIndex == -1) {
                bufferIndex = videoCreator.dequeueInputBuffer(20);
            }
            // notify codec of end of input stream
            videoCreator.queueInputBuffer(bufferIndex,0,0,0,BUFFER_FLAG_END_OF_STREAM);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

















    public void saveImage(Bitmap bitmapImage) {
        Log.i("VIDEO RECORD", "Grabbed image");
        videoArray.add(bitmapImage);
    }






    public ImageReader.OnImageAvailableListener createOnImageAvailableListener(ImageView imageView) {

        final ImageView targetImageView = imageView;  // the target of our listener; used below, when we update UI

        return new ImageReader.OnImageAvailableListener() {

            private Image newestImage;  // stores newest image from ImageReader, by reference
            private Image.Plane newestImageAsPlane;
            private int threadLock = 0;  // a lock on the creation of new threads; only want to make threads when needed


            ////////

            @Override
            public void onImageAvailable(ImageReader imageReader) {

                // grab the newest image that's come in
                newestImage = imageReader.acquireLatestImage();
                // give it name accessible to whole activity
                imageToSave = newestImage;

                // if we're ready to put a new image up on the screen,...
                if (threadLock == 0) {
                    // create a new (anonymous) thread
                    new Thread() {
                        public void run() {
                            try {

                                // turn on the thread lock, to prevent other threads from being created until we're done
                                threadLock = 1;

                                // get newestImage as a plane (0 is all we need)
                                newestImageAsPlane = newestImage.getPlanes()[0];
                                // get buffer for plane
                                ByteBuffer newestImageBuffer = newestImageAsPlane.getBuffer();
                                // get byte array that is same size as image buffer
                                byte[] bytes = new byte[newestImageBuffer.capacity()];
                                //grab bytes from buffer and transfer to byte array, one byte at a time
                                //  (no bulk transfer method from ByteBuffer to Byte array; have to to it like this)
                                for (int i = 0; i < newestImageBuffer.capacity(); i++) { bytes[i] = newestImageBuffer.get(i); }
                                // get our buffer as bitmap image
                                Bitmap bitmapImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                // rotate camera feed to undo camera sensor orientation (90, 180, 720 rotation)
                                Matrix matrix = new Matrix();
                                matrix.postRotate(cameraSensorFeedOrientation);
                                // fix orientation rotation
                                bitmapImage = Bitmap.createBitmap(bitmapImage, 0, 0, bitmapImage.getWidth(), bitmapImage.getHeight(), matrix, false);


                                // set the bitmap variable for scans in main thread to the bitmap we
                                // just created (so we have the option of saving it when "taking a scan"
                                newestScanImage = bitmapImage;  // we are assigning this to a variable accessible to *all of*
                                                                // the activity, not just local to the thread or the listener, so
                                                                // we can grab it


                                // update the imageView on the main thread
                                uiHandler.post(new UIImageViewUpdate(bitmapImage, targetImageView));
                                // close out the image to free up space for imageReader
                                newestImage.close();

                                // open up thread lock again
                                threadLock = 0;

                            } catch (IllegalStateException e) {
                                // still close out the image on error
                                newestImage.close();
                                // open up thread lock again, so we can keep getting images
                                threadLock = 0;
                                e.printStackTrace();
                            }
                        }
                    }.start();

                    // if thread lock is *on*,...
                } else {
                    // just dump the image
                    newestImage.close();
                }

            }

            ///////////

        };

    }






    public class UIImageViewUpdate implements Runnable {
        ////////////////////////////////////////
        // This is a runnable that we will spin up instances of & use to update the UI/main thread
        //

        Bitmap previewBitmap;
        ImageView previewView;

        UIImageViewUpdate(Bitmap previewBitmap, ImageView previewView) {
            this.previewBitmap = previewBitmap;
            this.previewView = previewView;
        }

        @Override
        public void run() {
            previewView.setImageBitmap(previewBitmap);
        }

    }





}
