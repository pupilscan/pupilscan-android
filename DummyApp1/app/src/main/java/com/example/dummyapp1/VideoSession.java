package com.example.dummyapp1;


import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;
import java.util.ArrayList;


public class VideoSession {



    // INSTANCE VARIABLES

    private CaptureRequest.Builder cameraCaptureRequest;
    private ArrayList<Surface> surfaceList = new ArrayList<Surface>();
    private CameraManager cameraManager;
    private CameraDevice cameraDevice;
    private CameraCaptureSession videoCaptureSession;
    private String cameraName;
    private CameraCaptureSession.StateCallback videoDeviceSessionStateCallback;



    // CONSTRUCTOR


    public VideoSession(CameraManager cameraManager, String cameraName) {
        this.cameraManager = cameraManager;  // get the cameraManager we are using in the whole activity
        this.cameraName = cameraName;  // the name of the camera, as a string. We get this earlier from the CameraManager, via getCameraIdList
    }




    // METHODS


    private void createNewCaptureSessionStateCallback() {
        //////////////////////////////////////////////////
        // This method will create for us a new set of callbacks, and what to do
        //

        // create the callbacks for the session
        videoDeviceSessionStateCallback = new CameraCaptureSession.StateCallback() {
            @Override
            public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                // grab the CameraCaptureSession by reference
                videoCaptureSession = cameraCaptureSession;  // this is a variable for the entire VideoSession object
                // build the request
                CaptureRequest builtCaptureRequest = cameraCaptureRequest.build();
                try {
                    // set to make continual requests
                    videoCaptureSession.setRepeatingRequest(builtCaptureRequest, null, null);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                Log.i("CAMERACAPTURESESSION", "Configuration failed.");
            }
        };

    }



    public void toggleFlashState(boolean desiredFlashState) {
        ////////////////////////////
        // Toggle flash state on and off
        //  (true for on, false for off)
        //


        // Here we will make a new capture request with the desired configuration:

        // turn on or off flash, as requested
        if (desiredFlashState==true) {
            cameraCaptureRequest.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH);
        } else {
            cameraCaptureRequest.set(CaptureRequest.FLASH_MODE,CaptureRequest.FLASH_MODE_OFF);
        }
        // build the request
        CaptureRequest builtCaptureRequest = cameraCaptureRequest.build();
        try {
            // stop the repeating request that currently exists
            videoCaptureSession.stopRepeating();
            // create a new repeating request with the new configuration
            videoCaptureSession.setRepeatingRequest(builtCaptureRequest, null, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }




    public void turnOnVideoCamera(ArrayList<Surface> surfaceList) {
        ///////////////////////////
        //
        //

        // get the surfaces to be associated with this VideoSession instance. These will be needed
        // once the camera is opened, b/c of videoDeviceStateCallback.onOpened()
        this.surfaceList = surfaceList;

        // open up the camera
        try {
            cameraManager.openCamera(cameraName, videoDeviceStateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }


    public void turnOffVideoCamera() {
        cameraDevice.close();
        Log.i("VideoCamera", "Camera is now off");
    }




    public void startCapture(boolean desiredTorchState) {
        //////////////////////////
        //
        //


        try {
            // create a capture request
            cameraCaptureRequest = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }



        // add all the needed surfaces
        for (int i=0; i < surfaceList.size(); i++ ) {
            // add as a target of our capture request
            cameraCaptureRequest.addTarget(surfaceList.get(i));
        }



        // turn off autofocus
        cameraCaptureRequest.set(CaptureRequest.CONTROL_AF_MODE,CaptureRequest.CONTROL_AF_MODE_OFF);

        // set torch state to desired state
        if (desiredTorchState == true) {
            cameraCaptureRequest.set(CaptureRequest.FLASH_MODE,CaptureRequest.FLASH_MODE_TORCH);
        } else {
            cameraCaptureRequest.set(CaptureRequest.FLASH_MODE,CaptureRequest.FLASH_MODE_OFF);
        }


        createNewCaptureSessionStateCallback();  // this gets us the videoDeviceSessionStateCallback object


        Handler newCaptureSessionHandler = new Handler();  // a named handler for the session (idk why we need this, may not)




        // create the actual capture session
        try {
            cameraDevice.createCaptureSession(surfaceList, videoDeviceSessionStateCallback, newCaptureSessionHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }



    }










    // OBJECTS


    private CameraDevice.StateCallback videoDeviceStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice camera) {
            // grab the camera (we need to grab it so we can toggle flash state later)
            cameraDevice = camera;
            // start with the torch off by default
            startCapture(false);
        }

        @Override
        public void onClosed(CameraDevice camera) { Log.i("CameraCallback", "CAMERA CLOSED"); }
        @Override
        public void onDisconnected(CameraDevice camera) { Log.i("CameraCallback", "CAMERA DISCONNECTED"); }
        @Override
        public void onError(CameraDevice camera, int error) { Log.i("CameraCallback", "CAMERA ERROR"); }

    };




}

