package com.example.dummyapp1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.content.pm.ActivityInfo;
import android.graphics.Color;

import android.os.Bundle;
import android.view.WindowManager;


public class MainActivity extends AppCompatActivity {



    //////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        // UI SETUP


        // hide actionbar
        getSupportActionBar().hide();

        // grab toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        // set the logo
        toolbar.setLogo(R.drawable.heading_200);
        // set the color
        toolbar.setBackgroundColor(Color.rgb(0,0,0));



        // Set & lock screen settings:
        // (prevent camera from crashing when screen is rotated)

        // put it in portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // lock it in portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        // prevent screen from dimming
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);







    }
    ////////////////////////////////////////////////////////////















}
